<?php

return [
    'font_path' => base_path('resources/fonts/'),
    'font_data' => [
        'DIN' => [
            'R'  => 'FF_DIN_Pro_Regular.otf',    // regular font

        ]
    ],
    'mode'                  => 'utf-8',
    'format'                => 'A4',
    'author'                => 'Insibe Technologies Co.WLL',
    'subject'               => '',
    'keywords'              => '',
    'creator'               => 'Laravel Pdf',
    'display_mode'          => 'fullwidth',
    'tempDir'               => base_path('../temp/')
];