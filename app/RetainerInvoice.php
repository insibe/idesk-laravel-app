<?php

namespace App;
use Auth;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class RetainerInvoice extends Model
{
    protected $fillable = ['user_id', 'client_id', 'proposal_id', 'rt_number', 'rt_date', 'rt_amount', 'rtPayment_type', 'rt_reference', 'rt_notes', 'currency_id', 'rt_status', 'last_sent_date'];

    protected $dates = ['rt_date','created_at', 'last_sent_date','updated_at','deleted_at'];


    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function getRtDateAttribute($date)
    {
        return Carbon::parse($date)->format('d F, Y');
    }

    public function getLastSentDateAttribute($date)
    {
        return Carbon::parse($date)->format('d M Y');
    }

    public function paymentType()
    {
        $rtPayment_type = '0';
        if ($this->rtPayment_type=='0') {
            $rtPayment_type = '<span class="badge badge-secondary">CHEQUE DEPOSIT</span>';
        } elseif ($this->rtPayment_type=='1' ) {
            $rtPayment_type = '<span class="badge badge-primary">BANK TRANSFER</span>';
        } elseif ($this->rtPayment_type=='2' ) {
            $rtPayment_type = '<span class="badge badge-success">BY CASH </span>';
        }elseif ($this->rtPayment_type=='3') {
            $rtPayment_type = '<span class="badge badge-danger">PAID</span>';
        }elseif ($this->rtPayment_type=='4') {
            $rtPayment_type = '<span class="badge badge-warning">CANCELLED</span>';
        }

        echo $rtPayment_type;
    }

    public function status()
    {
        $status = '0';
        if ($this->rt_status=='0') {
            $status = '<span class="badge badge-secondary">DRAFT</span>';
        } elseif ($this->rt_status=='1' ) {
            $status = '<span class="badge badge-primary">SENT</span>';
        } elseif ($this->rt_status=='2' ) {
            $status = '<span class="badge badge-success">PARTIALLY PAID</span>';
        }elseif ($this->rt_status=='3') {
            $status = '<span class="badge badge-danger">PAID</span>';
        }elseif ($this->rt_status=='4') {
            $status = '<span class="badge badge-warning">CANCELLED</span>';
        }
        echo $status;
    }

}
