<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProposalItems extends Model
{
    protected $fillable = ['proposal_id', 'title', 'description', 'price'];

    protected $dates = ['proposal_date','created_at', 'last_sent_date','updated_at','deleted_at'];



}
