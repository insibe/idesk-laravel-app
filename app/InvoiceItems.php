<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceItems extends Model
{
    protected $fillable = ['invoice_id', 'invoice_item', 'invoice_itemDsc', 'invoice_itemQty', 'invoice_itemPrice','invoice_itemTotal'];

    protected $dates = ['created_at','updated_at','deleted_at'];

    public function invoice()
    {
        return $this->belongsTo(invoice::class);
    }
}
