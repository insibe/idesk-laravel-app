<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = ['user_id', 'client_id', 'retainer_id', 'invoice_id', 'amount', 'payment_type', 'payment_reference', 'payment_notes', 'currency_id', 'payment_status', 'last_sent_date'];

    protected $dates = ['paid_at','created_at', 'last_sent_date','updated_at','deleted_at'];
}
