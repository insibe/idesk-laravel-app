<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\Payment;
use App\client;
use Auth;
use App\RetainerInvoice;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payments = Payment::all();
        $data = [

            'page_title' => 'Manage Payments'
        ];

        return view('dashboard.payments.index',compact('payments'),$data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $invoiceDetails = Invoice::findOrFail($id);
        $data = [
            'payment' => $invoiceDetails,
            'formMethod' => 'POST',
            'url' => 'dashboard/payments/',
            'page_title' => 'Payment for Invoice',
            'rt_date'=> Carbon::now()->format('d F, Y'),
            'rt_due'=> Carbon::now()->format('d F, Y'),
            'rt_number' => 'INV2001'
        ];

        return view('dashboard.payments.edit',compact('payment') ,$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $currencyID = '1';
        $payment = Payment::create([
            'client_id' => $request->input('client_id'),
            'user_id' => Auth::guard()->user()->id,
            'proposal_id' => $request->input('proposal_id'),
            'invoice_id' => $request->input('invoice_id'),
            'paid_at' => Carbon::parse($request->rt_date),
            'payment_reference' => $request->input('rt_reference'),
            'payment_notes' => $request->input('rt_notes'),
            'payment_type' => $request->input('payment_type'),
            'amount' => $request->input('due_amount'),
            'payment_status' => '1',
            'currency_id' => $currencyID,
            'last_sent_date' => Carbon::now(),
            //
        ]);

        $invoice = Invoice::findOrFail($payment->invoice_id);
        $amountTotal = ($invoice->amount_paid)+($payment->amount);
        $invoice->update([
            'amount_paid'=>$amountTotal ,
            'due_amount' => ($invoice->grandTotal - $amountTotal),
            'inv_status' => '3'
        ]);



        return redirect('dashboard/payment/'.$payment->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
