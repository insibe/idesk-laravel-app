<?php

namespace App\Http\Controllers;
use App\RetainerInvoice;
use Auth;
use App\Invoice;
use App\client;
use App\proposal;
use Carbon\Carbon;
use Illuminate\Http\Request;
use PDF;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoices =Invoice::all();
        $data = [

            'page_title' => 'Manage Invoices'
        ];

        return view('dashboard.invoices.index',compact('invoices'),$data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = Client::pluck('primary_contact', 'id');
        $invoiceNumber = Invoice::latest()->first();

        if($invoiceNumber == null){
            $invoiceNumber = '10001';
        }else{
            $invoiceNumber = (substr($invoiceNumber->inv_number,-5))+1;

        }



        $data = [
            'invoice' => null,
            'formMethod' => 'POST',
            'url' => 'dashboard/invoices',
            'page_title' => 'Create Invoice',
            'inv_date'=> Carbon::now()->format('d F, Y'),
            'inv_due'=> Carbon::now()->format('d F, Y'),
            'inv_number' => 'INV'.$invoiceNumber ,
        ];

        return view('dashboard.invoices.edit',compact('clients') ,$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $currencyID = '1';

        $invoice = Invoice::create([
            'client_id' => $request->input('client_id'),
            'user_id' => Auth::guard()->user()->id,
            'inv_number' => $request->input('inv_number'),
            'pro_number' => $request->input('pro_number'),
            'inv_date' => Carbon::parse($request->invoice_date),
            'inv_due' => Carbon::parse($request->invoice_due),
            'inv_type' => '1',
            'inv_discount' => $request->input('inv_discount'),
            'inv_total' => $request->input('inv_total'),
            'grandTotal' => $request->input('grandTotal'),
            'amount_paid'=>'0.000',
            'due_amount'=>'0.000',
            'inv_status' => '1',
            'version_id' => '1',
            'currency_id' => $currencyID,
            'last_sent_date' => Carbon::now(),
        ]);


        foreach ($request->invoiceItems as $item)
            {
                $data = array(
                    'proposal_id' => $invoice->id,
                    'invoice_item' => $item['item_title'],
                    'invoice_itemDsc' => $item['item_description'],
                    'invoice_itemQty' => $item['item_qty'],
                    'invoice_itemPrice' => $item['item_price'],
                    'invoice_itemTotal' => $item['item_total'],
                );
                $invoice->invoiceItems()->create($data);
            }


        return redirect('dashboard/invoices/'.$invoice->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $invoice = Invoice::findOrFail($id);
        return view('dashboard.invoices.show', compact('invoice'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $invoice = Invoice::findOrFail($id);
        $clients = Client::pluck('primary_contact', 'id');
        $data = [
            'invoice' => $invoice,
            'formMethod' => 'PUT',
            'url' => 'dashboard/invoices/'.$id,
            'page_title' => ' Edit '.$invoice->title
        ];

        return view('dashboard.invoices.edit',compact('invoice','clients'),$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $currencyID = '1';
        $invoice = Invoice::findOrFail($id);
        $dueAmount =  ($request->input('grandTotal')-($invoice->amount_paid) );
        $invoice->update([
            'client_id' => $request->input('client_id'),
            'user_id' => Auth::guard()->user()->id,
            'inv_number' => $request->input('inv_number'),
            'pro_number' => $request->input('pro_number'),
            'inv_date' => Carbon::parse($request->invoice_date),
            'inv_due' => Carbon::parse($request->invoice_due),
            'inv_type' => '1',
            'inv_discount' => $request->input('inv_discount'),
            'inv_total' => $request->input('inv_total'),
            'grandTotal' => $request->input('grandTotal') ,
            'amount_paid'=>$invoice->amount_paid,
            'due_amount'=> $dueAmount,
            'inv_status' => '1',
            'version_id' => '1',
            'currency_id' => $currencyID,
            'last_sent_date' => Carbon::now(),
        ]);

        $invoice->invoiceItems()->delete();
        foreach ($request->invoiceItems as $item)
        {
            $data = array(
                'proposal_id' => $invoice->id,
                'invoice_item' => $item['item_title'],
                'invoice_itemDsc' => $item['item_description'],
                'invoice_itemQty' => $item['item_qty'],
                'invoice_itemPrice' => $item['item_price'],
                'invoice_itemTotal' => $item['item_total'],
            );
            $invoice->invoiceItems()->create($data);
        }
        return redirect('dashboard/invoices/'.$invoice->id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function paymentApply($invoiceId, $retainerID)
    {
        $invoice = Invoice::findOrFail($invoiceId);
        $retainerInvoice = RetainerInvoice::findOrFail($retainerID);
        $amountTotal = ($invoice->amount_paid)+($retainerInvoice->rt_amount);
        $invoice->update([
            'amount_paid'=>$amountTotal ,
            'due_amount' => ($invoice->grandTotal - $amountTotal),
        ]);

        $retainerInvoice->update([
            'rt_status'=>'2',
        ]);

        return redirect('dashboard/invoices/'.$invoice->id);

    }



    public function downloadPDF($id)
    {

        $invoice = Invoice::findOrFail($id);
        $pdf = PDF::loadView('dashboard.templates.invoice', ['invoice' =>  $invoice]);
        return $pdf->stream('fff.pdf');



    }


   

}
