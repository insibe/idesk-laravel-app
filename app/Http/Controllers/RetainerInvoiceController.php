<?php

namespace App\Http\Controllers;
use Auth;
use App\RetainerInvoice;
use App\client;
use Carbon\Carbon;
use Illuminate\Http\Request;
use PDF;


class RetainerInvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $retainerInvoices = RetainerInvoice::all();

        $data = [

            'page_title' => 'Manage Retainer Invoices'
        ];

        return view('dashboard.retainerInvoice.index',compact('retainerInvoices'),$data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = Client::pluck('primary_contact', 'id');
        $retainerNumber = RetainerInvoice::latest()->first();

        if($retainerNumber == null){
            $retainerNumber = '10001';
        }else{
            $retainerNumber = (substr($retainerNumber->rt_number,-5))+1;

        }

        $data = [
            'retainerInvoice' => null,
            'formMethod' => 'POST',
            'url' => 'dashboard/retainerInvoices',
            'page_title' => 'Create Retainer Invoice',
            'rt_date'=> Carbon::now()->format('d F, Y'),
            'rt_number' => 'RT'.$retainerNumber
        ];

        return view('dashboard.retainerInvoice.edit',compact('clients') ,$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $currencyID = '1';
        $retainerInvoice = RetainerInvoice::create([
            'client_id' => $request->input('client_id'),
            'user_id' => Auth::guard()->user()->id,
            'proposal_id' => $request->input('proposal_id'),
            'rt_number' => $request->input('rt_number'),
            'rt_date' => Carbon::parse($request->rt_date),
            'rt_reference' => $request->input('rt_reference'),
            'rt_notes' => $request->input('rt_notes'),
            'rtPayment_type' => $request->input('rtPayment_type'),
            'rt_amount' => $request->input('rt_amount'),
            'rt_status' => '1',
            'currency_id' => $currencyID,
            'last_sent_date' => Carbon::now(),
            //
        ]);

        return redirect('dashboard/retainerInvoices/'.$retainerInvoice->id);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $retainerInvoice = RetainerInvoice::findOrFail($id);
        $data = [
            'page_title' => 'id',
        ];
        return view('dashboard.retainerInvoice.show', compact('retainerInvoice',$data));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $retainerInvoice = RetainerInvoice::findOrFail($id);
        $clients = Client::pluck('primary_contact', 'id');
        $data = [
            'retainerInvoice' => $retainerInvoice,
            'formMethod' => 'PUT',
            'url' => 'dashboard/retainerInvoices/'.$id,
            'page_title' => ' Edit '.$retainerInvoice->title
        ];

        return view('dashboard.retainerInvoice.edit',compact('retainerInvoice','clients'),$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $currencyID = '1';
        $retainerInvoice = RetainerInvoice::findOrFail($id);

        $retainerInvoice->update([
            'client_id' => $request->input('client_id'),
            'user_id' => Auth::guard()->user()->id,
            'proposal_id' => $request->input('proposal_id'),
            'rt_number' => $request->input('rt_number'),
            'rt_date' => Carbon::parse($request->rt_date),
            'rt_reference' => $request->input('rt_reference'),
            'rt_notes' => $request->input('rt_notes'),
            'rtPayment_type' => $request->input('rtPayment_type'),
            'rt_amount' => $request->input('rt_amount'),
            'rt_status' => '0',
            'currency_id' => $currencyID,
            'last_sent_date' => Carbon::now(),
        ]);
        return redirect('dashboard/retainerInvoices/'.$retainerInvoice->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function downloadPDF($id)
    {
        $retainerInvoice = RetainerInvoice::findOrFail($id);
        $pdf = PDF::loadView('dashboard.templates.receipt', ['retainerInvoice' =>  $retainerInvoice]);
        return $pdf->stream($retainerInvoice->id.'.pdf');
    }

}
