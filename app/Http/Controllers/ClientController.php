<?php

namespace App\Http\Controllers;


use App\client;
use App\contact;
use App\Http\Requests\clientRequest;
class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients =client::with('contacts')->paginate(10);
        $data = [

            'page_title' => 'Manage Clients'
        ];

        return view('dashboard.clients.index',compact('clients'),$data );

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $data = [
            'clients' => null,
            'formMethod' => 'POST',
            'url' => 'dashboard/clients',
            'page_title' => 'Add New Client'
        ];

        return view('dashboard.clients.edit',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(clientRequest $request)
    {

        //dd( $request->all());
        $input = $request->all();
        $input['user_id'] = '1';
        $input['client_type'] = '1';
        $input['currency_id'] = '1';
        $client = Client::create($input);
        $user_id = auth()->id();
//
//        $contacts = collect($request->contact);
//
//        $contactsArry =  $contacts->toArray();
        return redirect('dashboard/clients/'.$client->id);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $contact = Client::findOrFail($id);
        return view('dashboard.clients.show', compact('contact'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = client::with('contacts')->findOrFail($id);

        $data = [
            'clients' => $client,
            'formMethod' => 'PUT',
            'url' => 'dashboard/clients/'.$id,
            'page_title' => ' Edit '.$client->title
        ];

        return view('dashboard.clients.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(clientRequest $request, $id)
    {
        $input = $request->all();
        $client = Client::findOrFail($id);
        $client->update($input);


        return redirect('dashboard/clients/'.$client->id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $client = client::findOrFail($id);

        $client->delete();



        return redirect()->route('dashboard.clients.index');
    }
}
