<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Auth;
use App\client;
use App\proposal;
use Carbon\Carbon;
use App\ProposalAttachments;
use Illuminate\Support\Facades\App;
use PDF;




class ProposalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proposals =Proposal::all();
        $data = [

            'page_title' => 'Manage Proposals'
        ];

        return view('dashboard.proposals.index',compact('proposals'),$data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = Client::pluck('primary_contact', 'id');
        $proposalNumber = Proposal::latest()->first();

        if($proposalNumber == null){
            $proposalNumber = '212';
        }else{
            $proposalNumber = (substr($proposalNumber->pro_number,-5))+1;

        }

        $data = [
            'proposal' => null,
            'formMethod' => 'POST',
            'url' => 'dashboard/proposals',
            'page_title' => 'Create Proposal',
            'pro_date'=> Carbon::now()->format('d F, Y'),
            'pro_number' => 'BH'.Carbon::now()->format('y').$proposalNumber
        ];

        return view('dashboard.proposals.edit',compact('clients') ,$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $currencyID = '1';
        $proposal = Proposal::create([
            'client_id' => $request->input('client_id'),
            'user_id' => Auth::guard()->user()->id,
            'acntManager_id' => $request->input('acntManager_id'),
            'pro_number' => $request->input('pro_number'),
            'pro_date' => Carbon::parse($request->pro_date),
            'pro_title' => $request->input('pro_title'),
            'proposal_category' => $request->input('pro_category'),
            'proposal_type' => '1',
            'pro_grandTotal' => $request->input('amount'),
            'pro_status' => '1',
            'currency_id' => $currencyID,
            'last_sent_date' => Carbon::now(),
            //
        ]);

        return redirect('dashboard/proposals/'.$proposal->id);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $proposal = Proposal::findOrFail($id);
        return view('dashboard.proposals.show', compact('proposal'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proposal = Proposal::findOrFail($id);
        $clients = Client::pluck('primary_contact', 'id');
        $data = [
            'proposals' => $proposal,
            'formMethod' => 'PUT',
            'url' => 'dashboard/proposals/'.$id,
            'page_title' => ' Edit '.$proposal->title
        ];

        return view('dashboard.proposals.edit',compact('proposal','clients'),$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

     //dd($request->input('proposal_status'));
        $currencyID = '1';
        $proposal = Proposal::findOrFail($id);
        $proposal->update([
            'client_id'=>$request->input('client_id'),
            'user_id'=>Auth::guard()->user()->id,
            'proposal_date'=>Carbon::parse($request->proposal_date),
            'proposal_title'=>$request->input('proposal_title'),
            'proposal_content'=>$request->input('proposal_content'),
            'proposal_terms'=>'terms',
            'proposal_category'=>'1',
            'proposal_type'=>'1',
            'proposal_features'=>'1',
            'discount' => $request->input('discount'),
            'subtotal' => $request->input('subtotal'),
            'amount' => $request->input('amount'),
            'proposal_status'=>$request->input('proposal_status'),
            'version_id'=>'1',
            'currency_id'=>$currencyID,
            'last_sent_date'=>Carbon::now(),
            //
        ]);

         redirect('dashboard/proposals/'.$proposal->id);
        //return redirect()->route('dashboard.proposals.show', [ $id ]);

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function generatePDF($id)
    {

        $proposal = Proposal::findOrFail($id);
        $pdf = PDF::loadView('dashboard.templates.proposal', ['proposal' => $proposal]);

        return $pdf->stream('document.pdf');
    }

    public function downloadTemplate()
    {

        $proposalNumber = Proposal::latest()->first();

        if($proposalNumber == null){
            $proposalNumber = '212';
        }else {
            $proposalNumber = (substr($proposalNumber->pro_number, -5)) + 1;
        }

        $file= public_path(). "/templates/proposal_wordpress.pptx";

        $headers = array(
            'Content-Type: application/pdf',
        );

        return  response()->download($file, $proposalNumber.'.pptx', $headers);

    }


    public function storeAttachment(Request $request,$id)
    {

        $proposal = Proposal::findOrFail($id);
        if($request->hasFile('file')) {
            $file = $request->file('file');
            $destinationPath = public_path () . '/uploads/proposals';
            $filename = $file ->getClientOriginalName();
            $image = time().$proposal->pro_number;
            $file ->move($destinationPath, $image.'.' .$file->getClientOriginalExtension());
            $imgpath = 'uploads/proposals/' . $image.'.' .$file->getClientOriginalExtension();


            }



            $proposalAttachment = ProposalAttachments::create([
                'user_id' => Auth::guard()->user()->id,
                'proposal_id' => $proposal->id,
                'version_id' => '1',
                'status' => '1',
                'path' => $imgpath,
                'title' => $proposal->pro_number,
                'size' => $request->file('file')->getClientSize(),
                'type' => $request->file('file')->getClientMimeType(),
                'last_sent_date' => Carbon::now(),
                //
            ]);
        return response()->json(['Status'=>true, 'Message'=>'Image(s) Uploaded.']);
    }

    public function getAttachment($id)
    {
        $proposal = Proposal::findOrFail($id);

        return response()->json([$proposal->proposalAttachments ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
