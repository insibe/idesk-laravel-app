<?php

namespace App\Http\Controllers;

use App\ProposalItems;
use Illuminate\Http\Request;

class ProposalItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProposalItems  $proposalItems
     * @return \Illuminate\Http\Response
     */
    public function show(ProposalItems $proposalItems)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProposalItems  $proposalItems
     * @return \Illuminate\Http\Response
     */
    public function edit(ProposalItems $proposalItems)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProposalItems  $proposalItems
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProposalItems $proposalItems)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProposalItems  $proposalItems
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProposalItems $proposalItems)
    {
        //
    }
}
