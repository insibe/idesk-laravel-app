<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Carbon\Carbon;


class Proposal extends Model
{
    protected $fillable = ['last_sent_date','pro_date','acntManager_id','user_id', 'client_id', 'pro_number', 'pro_title', 'pro_category', 'pro_type', 'pro_grandTotal', 'pro_status', 'currency_id','currency_id'];

    protected $dates = ['created_at','updated_at','deleted_at'];




    public function client()
    {
        return $this->belongsTo(Client::class);
    }


    public function setPublishedAtAttribute($date)
    {
        $this->attributes['pro_date'] = Carbon::parse($date);
    }


    public function getProDateAttribute($date)
    {
        return Carbon::parse($date)->format('d F, Y');
    }


    public function attachedProposals()
    {
        $attachedProposals = array();

        if($this->ProposalAttachments){
            foreach($this->ProposalAttachments as $attachment){
              $attachedProposals[] = url($attachment->path);
              }
        }
        echo json_encode($attachedProposals,JSON_UNESCAPED_SLASHES);

    }



    public function attachmentsPreviewConfig()
    {
        $attachmentsPreviewConfig = array();
        $i = 0;
        if($this->ProposalAttachments){
            foreach($this->ProposalAttachments as $attachment){

                 $attachmentsPreviewConfig[$i]['type'] = $attachment->type;
                 $attachmentsPreviewConfig[$i]['size'] = $attachment->size;
                 $attachmentsPreviewConfig[$i]['caption'] = $attachment->title;
                 $attachmentsPreviewConfig[$i]['filename'] = '1533463749BH18212.pdf';
                 $attachmentsPreviewConfig[$i]['url'] = $attachment->path;
                 $attachmentsPreviewConfig[$i]['key'] = $attachment->id;
                 $attachmentsPreviewConfig[$i]['status'] = $attachment->id;
                 $i++;
            }
        }

        echo json_encode($attachmentsPreviewConfig,JSON_UNESCAPED_SLASHES);





    }



    public function proposalAttachments()
    {
        return $this->hasMany(ProposalAttachments::class);

    }




    public function status()
    {
        $status = '0';

        if ($this->pro_status=='0') {
        $status = '<span class="badge badge-secondary">DRAFT</span>';
         } elseif ($this->pro_status=='1' ) {
         $status = '<span class="badge badge-primary">IN DISCUSSION</span>';
         } elseif ($this->pro_status=='2' ) {
         $status = '<span class="badge badge-success">WON</span>';
        }elseif ($this->pro_status=='3') {
            $status = '<span class="badge badge-danger">REJECTED</span>';
        }elseif ($this->pro_status=='4') {
            $status = '<span class="badge badge-warning">CANCELLED</span>';
        }

        echo $status;
    }





}
