<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    protected $table = 'client_contacts';

    protected $fillable = ['user_id'.'client_id','first_name','last_name','contact_email','contact_phone','contact_department'];

    public function Client()
    {
        return $this->belongsTo('App\Client');
    }
}
