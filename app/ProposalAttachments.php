<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProposalAttachments extends Model
{
    protected $fillable = ['user_id', 'proposal_id','version_id', 'path','title','type','size','mime','status'];

    protected $dates = ['last_sent_date','created_at','updated_at','deleted_at'];

    public function proposal()
    {
        return $this->belongsTo(Proposal::class);
    }





}
