<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{


    protected $fillable = [
        'primary_contact',
        'user_id',
        'client_type',
        'primary_contact',
        'company',
        'email',
        'work_phone',
        'mobile_phone',
        'website',
        'address1',
        'address2',
        'city',
        'state',
        'postal_code',
        'currency_id',
        'country_id',
 ];
    protected $dates = ['created_at', 'updated_at','deleted_at'];

    /**
     * A Cleent can have many contacts.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contacts()
    {
        return $this->hasMany('App\Contacts');

    }

    public function proposals()
    {
        return $this->hasMany(Proposal::class);

    }
    public function invoices()
    {
        return $this->hasMany(Invoice::class);

    }

    public function retainerInvoices()
    {
        return $this->hasMany(RetainerInvoice::class);

    }


    public function currency()
    {
        $currency = '1';
        if ($this->currency_id == '1') {
            $currency = 'BHD - Bahraini Dinar';
        }
        echo $currency;
    }

    public function country()
    {
        $country = '0';
        if ($this->country_id == '0') {
            $country = 'Bahrain';
        }
        echo $country;
    }

}
