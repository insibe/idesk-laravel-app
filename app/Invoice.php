<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Carbon\Carbon;

class Invoice extends Model
{
    protected $fillable = ['last_sent_date','inv_date','inv_due','user_id', 'client_id', 'inv_number',  'inv_type', 'inv_terms', 'inv_discount', 'inv_total',  'amount_paid',  'due_amount',  'grandTotal', 'inv_status', 'currency_id'];

    protected $dates = ['created_at','updated_at','deleted_at'];


    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function getInvDateAttribute($date)
    {
        return Carbon::parse($date)->format('d M Y');
    }
    public function getInvDueAttribute($date)
    {
        return Carbon::parse($date)->format('d M Y');
    }
    public function status()
    {
        $status = '0';
        if ($this->inv_status=='0') {
            $status = '<span class="badge badge-default">DRAFT</span>';
        } elseif ($this->inv_status=='1' ) {
            $status = '<span class="badge badge-primary">SENT</span>';
        } elseif ($this->inv_status=='2' ) {
            $status = '<span class="badge">PARTIALLY PAID</span>';
        }elseif ($this->inv_status=='3') {
            $status = '<span class="badge badge-success">PAID</span>';
        }elseif ($this->inv_status=='4') {
            $status = '<span class="badge badge-warning">CANCELLED</span>';
        }

        echo $status;
    }

    public function type()
    {
        $type = '0';
        if ($this->inv_type=='0') {
            $type = '<span class="badge badge-secondary">NORMAL</span>';
        }elseif ($this->inv_type=='1') {
            $type = '<span class="badge badge-warning">RECURRING</span>';
        }
        echo $type;
    }

    public function invoiceItems()
    {
        return $this->hasMany(InvoiceItems::class);
    }

}


