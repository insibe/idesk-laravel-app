<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposals', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('client_id')->unsigned();
            $table->integer('acntManager_id')->unsigned();
            $table->string('pro_number');
            $table->timestamp('pro_date');
            $table->longtext('pro_title')->nullable();
            $table->integer('pro_category')->nullable();
            $table->integer('pro_type')->nullable();;
            $table->decimal('pro_grandTotal', 10, 3)->nullable();
            $table->integer('currency_id')->unsigned();
            $table->integer('pro_status')->unsigned();
            $table->timestamp('last_sent_date')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('acntManager_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proposals');
    }
}
