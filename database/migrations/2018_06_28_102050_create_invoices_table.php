<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('client_id')->unsigned();
            $table->string('inv_number');
            $table->string('pro_number');
            $table->timestamp('inv_date');
            $table->timestamp('inv_due')->nullable();
            $table->integer('inv_type');
            $table->longtext('inv_terms');
            $table->decimal('inv_discount', 10, 3)->nullable();
            $table->decimal('inv_total', 10, 3);
            $table->decimal('grandTotal', 10, 3);
            $table->decimal('amount_paid', 10, 3);
            $table->decimal('due_amount', 10, 3)->nullable();
            $table->integer('version_id')->unsigned();
            $table->integer('currency_id')->unsigned();
            $table->integer('inv_status')->unsigned();
            $table->timestamp('last_sent_date')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
