<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRetainerInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retainer_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('client_id')->unsigned();
            $table->integer('proposal_id')->unsigned()->nullable();
            $table->string('rt_number');
            $table->timestamp('rt_date');
            $table->decimal('rt_amount', 10, 3);
            $table->integer('rtPayment_type');
            $table->text('rt_reference')->nullable();
            $table->longtext('rt_notes')->nullable();
            $table->integer('currency_id')->unsigned();
            $table->integer('rt_status');
            $table->timestamp('last_sent_date')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('proposal_id')->references('id')->on('proposals')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retainer_invoices');
    }
}
