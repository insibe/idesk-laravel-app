<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('client_id')->unsigned();
            $table->integer('retainer_id')->unsigned()->nullable();
            $table->integer('invoice_id')->unsigned();
           $table->timestamp('paid_at');
            $table->decimal('amount', 10, 3);
            $table->integer('payment_type');
            $table->text('payment_reference')->nullable();
            $table->longtext('payment_notes')->nullable();
            $table->integer('currency_id')->unsigned();
            $table->integer('payment_status');
            $table->timestamp('last_sent_date')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('retainer_id')->references('id')->on('retainer_invoices')->onDelete('cascade');
            $table->foreign('invoice_id')->references('id')->on('invoices')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
