<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/dashboard/proposals/{id}/generatePDF', 'ProposalController@generatePDF');
Route::get('/dashboard/invoices/{id}/downloadPDF', 'InvoiceController@downloadPDF');
Route::get('/dashboard/retainerInvoices/{id}/downloadPDF', 'RetainerInvoiceController@downloadPDF');



Route::group(['middleware' => 'dashboard'], function () {
    Route::get('/', function () {
        return view('home');
    });
    Route::get('/dashboard', 'HomeController@index')->name('dashboard');
    Route::resource('/dashboard/clients', 'ClientController');
    Route::resource('/dashboard/proposals', 'ProposalController');
    Route::get('/dashboard/proposal/downloadTemplate', 'ProposalController@downloadTemplate');
    Route::post('/dashboard/proposal/{proposalD}/storeAttachment', 'ProposalController@storeAttachment');
    Route::get('/dashboard/proposal/{proposalD}/getAttachment', 'ProposalController@getAttachment');
    Route::resource('/dashboard/invoices', 'InvoiceController');
    Route::get('/dashboard/invoices/{invoiceId}/paymentApply/{retainerId}', 'InvoiceController@paymentApply');
    Route::resource('/dashboard/retainerInvoices', 'RetainerInvoiceController');
    Route::resource('/dashboard/payments', 'PaymentController');
    Route::get('/dashboard/payments/{invoiceId}/create', 'PaymentController@create');






});

//Route::get('',array('as'=>'generatePDF','uses'=>'ProposalController@generatePDF'));





