@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                        <section class="content">
                            <div class="box box-solid">
                                <div class="box-header">
                                    <h3 class="box-title">Invoice Summary</h3>

                                    <div class="box-tools pull-right">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-calendar"></i> Year to Date
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a href="#" onclick="return false;" class="invoice-dashboard-total-change-option" data-id="year_to_date">Year to Date</a>
                                                </li>
                                                <li>
                                                    <a href="#" onclick="return false;" class="invoice-dashboard-total-change-option" data-id="this_quarter">This Quarter</a>
                                                </li>
                                                <li>
                                                    <a href="#" onclick="return false;" class="invoice-dashboard-total-change-option" data-id="all_time">All Time</a>
                                                </li>
                                                <li>
                                                    <a href="#" onclick="return false;" data-toggle="modal" data-target="#invoice-summary-widget-modal">Custom Date Range</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <button class="btn btn-box-tool create-invoice"><i class="fa fa-plus"></i> Create Invoice</button>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-12">
                                            <div class="small-box bg-yellow">
                                                <div class="inner">
                                                    <h3>$0.00</h3>

                                                    <p>Draft Invoices</p>
                                                </div>
                                                <div class="icon">
                                                    <i class="ion ion-edit"></i>
                                                </div>
                                                <a href="https://demo.fusioninvoice.com/invoices?status=draft" class="small-box-footer">
                                                    View Draft Invoices <i class="fa fa-arrow-circle-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-12">
                                            <div class="small-box bg-aqua">
                                                <div class="inner">
                                                    <h3>$0.00</h3>

                                                    <p>Sent Invoices</p>
                                                </div>
                                                <div class="icon">
                                                    <i class="ion ion-share"></i>
                                                </div>
                                                <a class="small-box-footer" href="https://demo.fusioninvoice.com/invoices?status=sent">
                                                    View Sent Invoices <i class="fa fa-arrow-circle-right"></i>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-md-12">
                                            <div class="small-box bg-red">
                                                <div class="inner">
                                                    <h3>$0.00</h3>

                                                    <p>Overdue Invoices</p>
                                                </div>
                                                <div class="icon"><i class="ion ion-alert"></i></div>
                                                <a class="small-box-footer" href="https://demo.fusioninvoice.com/invoices?status=overdue">
                                                    View Overdue Invoices <i class="fa fa-arrow-circle-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-12">
                                            <div class="small-box bg-green">
                                                <div class="inner">
                                                    <h3>$0.00</h3>

                                                    <p>Payments Collected</p>
                                                </div>
                                                <div class="icon"><i class="ion ion-heart"></i></div>
                                                <a class="small-box-footer" href="https://demo.fusioninvoice.com/payments">
                                                    View Payments <i class="fa fa-arrow-circle-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
