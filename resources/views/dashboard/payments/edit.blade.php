@extends('layouts.app')

@section('content')

@section('toolbar')


    <!-- Page header -->
    {!! Form::model($payment, array( 'method' => $formMethod, 'url' => $url, 'class'=>'horizontal-form  ' ,'data-parsley-validate','files' => 'true','no-validate', 'enctype'=>'multipart/form-data')) !!}
    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold"> {{ isset($page_title ) ? $page_title  : 'default' }}</span> - #{{$payment->inv_number}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none text-center text-md-left mb-3 mb-md-0">
                    <button type="submit" class="btn btn-lg bg-primary">Save </button>
            </div>
        </div>
    </div>
    <!-- /page header -->
@endsection

<div class="row">
    <div class="col-lg-12 col-lg-offset-1">
        <div class="card">
            <div class="card-body">
                <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <input type="hidden" name="invoice_id" value="{{$payment->id}}">
                                <input type="hidden" name="client_id" value="{{ $payment->client->id  }}">
                                <label class="col-form-label">Client</label>
                                {!! Form::text('client_name',$payment->client->primary_contact, ['class' => 'form-control','readonly','required' =>'required']) !!}

                            </div>

                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <label class="col-form-label">Payment Date  </label>
                                {!! Form::text('rt_date',  isset($payment->rt_date ) ? $payment->rt_date  : $rt_date , ['id'=>'datepicker','class' => 'form-control','required' =>'required']) !!}
                            </div>

                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <label class="col-form-label">Retainer Invoice No. </label>
                                {!! Form::text('rt_number',  isset($payment->rt_number ) ? $payment->rt_number  : $rt_number , ['id'=>'datepicker','class' => 'form-control','readonly','required' =>'required']) !!}
                            </div>

                        </div>
                    </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label class="col-form-label">Retainer Amount</label>
                            {!! Form::text('due_amount', null, ['placeholder'=>'0.00','class' => 'form-control','required' =>'required']) !!}

                        </div>

                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label class="col-form-label">Payment Type</label>
                            {!! Form::select('payment_type',['1','2','3'], null, ['class' => 'form-control selectbox','required' =>'required']) !!}

                        </div>

                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label class="col-form-label">Transaction Reference  </label>
                            {!! Form::text('payment_reference', null , ['id'=>'datepicker','class' => 'form-control','required' =>'required']) !!}
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label class="col-form-label">Description</label>
                            {!! Form::textarea('payment_notes', null, ['rows' => 2, 'cols' => 40,'class' => 'form-control selectbox','required' =>'required']) !!}
                        </div>

                    </div>

                </div>


                </div>



                <div class="card-footer">
                    <span class="text-muted"></span>
                </div>
        </div>
    </div>

</div>
    {!! Form::close() !!}



@endsection

