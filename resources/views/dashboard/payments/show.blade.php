@extends('layouts.app')

@section('content')

@section('toolbar')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Dashboard</span> - {{ isset($page_title ) ? $page_title  : 'default' }}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none text-center text-md-left mb-3 mb-md-0">
                <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                    <div class="btn-group mr-2" role="group" aria-label="First group">

                        @if ( $retainerInvoice->rt_status == 0)
                            <a class=" btn-lg btn btn-light" href="/dashboard/retainerInvoices/{{ $retainerInvoice->id }}/edit"><i class="far fa-edit"></i></a>
                        @endif
                    </div>
                    <div class="btn-group mr-2" role="group" aria-label="Second group">
                        <a  class=" btn-lg btn btn-light" href="/dashboard/retainerInvoices/{{ $retainerInvoice->id }}/downloadPDF"><i class="far fa-file-pdf"></i></a>


                    </div>
                    <div class="btn-group" role="group" aria-label="Third group">
                        <a  class="btn btn-lg bg-primary" href="/dashboard/retainerInvoices/create"><i class="icon-paperplane ml-2"></i> Send Receipt</a>
                    </div>
                </div>



            </div>
        </div>
    </div>
    <!-- /page header -->
@endsection
<div class="row justify-content-md-center">
    <div class="col-sm-9">
        <div class="card">
            <div class="card-header bg-transparent header-elements-inline">
                <h6 class="card-title">Retainer ID #{{$retainerInvoice->rt_number}}</h6>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="mb-4">
                            <img src="{{asset('dist/global_assets/images/logo-company.png')}}" class="mb-3 mt-2" alt="" style="width: 245px;">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="mb-4">
                            <div class="text-sm-right">
                                <ul class="list list-unstyled mb-0">
                                    <li><span class="font-weight-semibold">Insibe Technologies Co.WLL | CR No.117003-1</span></li>
                                    <li><span class="font-weight-semibold">PO Box 3045 | Kingdom of Bahrain</span></li>
                                    <li><span class="font-weight-semibold">M. +973 35928671 | 37233959</span></li>
                                    <li><span class="font-weight-semibold">E. mail@insibe.com | W. www.insibe.com</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <span class="text-muted">Bill To:</span>
                        <ul class="list list-unstyled mb-0">
                            @if ($retainerInvoice->client->primary_contact)
                                {!! $retainerInvoice->client->primary_contact!!} -
                            @endif
                            @if ($retainerInvoice->client->company )
                                <span>  {!! $retainerInvoice->client->company !!}</span><br>
                            @endif
                            @if ($retainerInvoice->client->address1 )
                                <span>  {!! $retainerInvoice->client->address1 !!}</span>,
                            @endif
                            @if ($retainerInvoice->client->address2 )
                                <span>  {!! $retainerInvoice->client->address2 !!}</span> <br>
                            @endif
                            @if ($retainerInvoice->client->address2 )
                                <span>  {!! $retainerInvoice->client->address2 !!}</span><br>
                            @endif
                            @if ($retainerInvoice->client->work_phone )
                                <span> P. {!! $retainerInvoice->client->work_phone  !!}</span> |
                            @endif
                            @if ($retainerInvoice->client->email )
                                <span> E. {!! $retainerInvoice->client->email  !!}</span> <br>
                            @endif
                        </ul>
                    </div>
                    <div class="col-sm-6">
                        <span class="text-muted">Invoice To:</span>
                        <ul class="list list-unstyled mb-0">
                            <table>
                                <tr><td width="100px">Retainer#</td><td class="font-weight-semibold">{{$retainerInvoice->rt_number}}</td></tr>
                                <tr><td>Date </td><td class="font-weight-semibold">{{$retainerInvoice->rt_date}}</td></tr>
                                <tr><td>Amount</td><td class="font-weight-semibold">{{$retainerInvoice->rt_amount}}</td></tr>
                            </table>

                        </ul>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-8">
                        <ul class="list list-unstyled font-weight-semibold" style="font-size: 18px;">
                            <table>
                                <tr><td width="300">Payment Date</td><td>{{$retainerInvoice->rt_number}}</td></tr>
                                <tr><td>Reference Number </td><td>{{$retainerInvoice->rt_date}}</td></tr>
                                <tr><td>Payment Mode</td><td>{{$retainerInvoice->rtPayment_type}}</td></tr>
                            </table>
                        </ul>
                    </div>
                    <div class="col-sm-4">
                       <div  class="retainerAmount text-center">
                           <p>Amount Received</p>
                           <h3 class="font-weight-semibold">BHD {{$retainerInvoice->rt_amount}}</h3>
                       </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">

                        @if ($retainerInvoice->rt_notes)
                            <h6>Notes </h6>
                            <p>  {!!$retainerInvoice->rt_notes!!}</p>
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
 @endsection
