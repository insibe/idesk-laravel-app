@extends('layouts.app')

@section('content')

@section('toolbar')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Dashboard</span> - {{ isset($page_title ) ? $page_title  : 'default' }}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none text-center text-md-left mb-3 mb-md-0">

            </div>
        </div>
    </div>
    <!-- /page header -->
@endsection
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <table class="table table-hover">
                    <thead>
                    <tr >
                        <th>Status</th>
                        <th>Retainer ID</th>
                        <th>Retainer Date</th>
                        <th>Client</th>
                        <th>Amount</th>
                        <th>Send Date</th>
                        <th>Options</th>
                    </tr>
                    </thead>

                    <tbody>
                    @if(count($payments) > 0)
                        @foreach ($payments as $payment)
                            <tr>
                                <td></td>
                                <td><a href="/dashboard/retainerInvoices/{{ $payment->id }}">{{ $payment->rt_number }} </a></td>
                                <td>{{ $payment->rt_date }}   </td>

                                <td>{{ $payment->amount }}   </td>
                                <td>{{ $payment->last_sent_date }}   </td>



                            </tr>
                        @endforeach
                    @else
                        <tr><td colspan="5" class="text-info text-center">No items found.</td></tr>
                    @endif
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>


@endsection
