@extends('layouts.app')

@section('header-scripts')


    <script src="{{asset('dist/global_assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script src="{{asset('dist/global_assets/js/demo_pages/form_select2.js')}}"></script>

    <style type="text/css">
        #invoiceItems{}
        #invoiceItems .form-control{height: 40px;    overflow: hidden;
            word-wrap: break-word;
            resize: horizontal;
            border: none;
            outline: none;
            border-radius: 5px;
            background: #FBFCFC;}

        #invoiceItems.table tr, .table th, .table td {
            transition: background-color ease-in-out 0.15s;
            border-bottom: 4px solid #fff;
        }
        #invoiceItems .table th, .table td {
            padding: 0;

        }
        #invoiceItems .form-control:focus{
          border: none !important;
            }
        #invoiceItems. deleteRow a {
            color:#b6bbc0;
        }
    </style>

@endsection

@section('content')

    @section('toolbar')
        <!-- Page header -->
        <div class="page-header">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Dashboard</span> - {{ isset($page_title ) ? $page_title  : 'default' }}</h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
                {!! Form::model($invoice, array( 'method' => $formMethod, 'url' => $url, 'class'=>'horizontal-form  ' ,'data-parsley-validate','files' => 'true','no-validate', 'enctype'=>'multipart/form-data')) !!}
                <div class="header-elements d-none text-center text-md-left mb-3 mb-md-0">
                    <div class="header-elements">
                        <button type="submit" class="btn btn-lg bg-primary">Save </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page header -->
    @endsection



<div class="row justify-content-md-center">
    <div class="col-sm-10">
        <div class="card">
            <div class="card-body">
                    <div class="row">
                        <div class="col-6">

                            <div class="form-group">
                                <label class="col-form-label">Client</label>

                                {!! Form::select('client_id',$clients, null, ['class' => 'form-control  select-search ','required' =>'required']) !!}
                            </div>

                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <label class="col-form-label">Invoice Date  </label>
                                {!! Form::text('inv_date',  isset($invoice->inv_date ) ? $invoice->inv_date  : $inv_date , ['id'=>'datepicker','class' => 'form-control','required' =>'required']) !!}
                            </div>
                            <div class="form-group">
                                <label class="col-form-label">Invoice Due  </label>
                                {!! Form::text('inv_due',  isset($invoice->inv_due ) ? $invoice->inv_due  : $inv_due , ['id'=>'datepicker','class' => 'form-control','required' =>'required']) !!}
                            </div>
                        </div>
                        <div class="col-3">

                            <div class="form-group">
                                <label class="col-form-label">Invoice # </label>
                                {!! Form::text('inv_number',isset($invoice->inv_number ) ? $invoice->inv_number  : $inv_number  , ['readonly','class' => 'form-control','required' =>'required']) !!}
                            </div>
                            <div class="form-group">
                                <label class="col-form-label">Proposal # </label>
                                {!! Form::text('pro_number',null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>


                </div>

                <table class="table table-responsive" id="invoiceItems">
                    <thead>
                    <tr >
                        <th>#</th>
                        <th>Items</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Qty</th>
                        <th>Total</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>


                    @if (!empty($invoice->invoiceItems))
                        <?php $count = 0; ?>

                        @foreach ($invoice->invoiceItems as $item )
                            <tr class="item-row">
                                <td align="center" align="middle" style="vertical-align: middle;">1</td>
                                <td><input type="hidden" name="invoiceItems[0][item_id]" value="0"> {!! Form::text('invoiceItems[0][item_title]',$item->invoice_item, ['placeholder'=>'Item Name','class' => 'form-control','required'=>'required'] ) !!}</td>
                                <td>{!! Form::textarea('invoiceItems[0][item_description]',$item->invoice_itemDsc, ['size' => '30x2','class' => 'form-control','placeholder'=>'Item Description','required'=>'required'] ) !!}</td>
                                <td>{!! Form::number('invoiceItems[0][item_price]',$item->invoice_itemPrice, ['class' => 'form-control item-cost','placeholder'=>'0.00','data-parsley-type'=>'number','required'=>'required'] ) !!}</td>
                                <td>{!! Form::number('invoiceItems[0][item_qty]',$item->invoice_itemQty, ['class' => 'form-control item-qty','data-parsley-type'=>'number','required'=>'required'] ) !!}</td>
                                <td>{!! Form::text('invoiceItems[0][item_total]',$item->invoice_itemTotal, ['id'=>'total','class' => 'form-control item-total','readonly','required'=>'required','placeholder'=>'0.00'] ) !!}</td>
                                @if ($count > 0)
                                    <td align="center" style="vertical-align: middle;"><div id="deleteItem"><a href="#" id=""><i class="remove far fa-trash-alt"></i></a></div></td>
                                @endif
                            </tr>
                            <?php $count++ ?>
                        @endforeach
                    @else
                        <tr class="item-row">
                            <td align="center" align="middle" style="vertical-align: middle;">1</td>
                            <td><input type="hidden" name="invoiceItems[0][item_id]" value="0"> {!! Form::text('invoiceItems[0][item_title]',NULL, ['placeholder'=>'Item Name','class' => 'form-control','required'=>'required'] ) !!}</td>
                            <td>{!! Form::textarea('invoiceItems[0][item_description]',NULL, ['size' => '30x2','class' => 'form-control','placeholder'=>'Item Description','required'=>'required'] ) !!}</td>
                            <td>{!! Form::number('invoiceItems[0][item_price]',NULL, ['class' => 'form-control item-cost','placeholder'=>'0.00','data-parsley-type'=>'number','required'=>'required'] ) !!}</td>
                            <td>{!! Form::number('invoiceItems[0][item_qty]',1, ['class' => 'form-control item-qty','data-parsley-type'=>'number','required'=>'required'] ) !!}</td>
                            <td>{!! Form::text('invoiceItems[0][item_total]',NULL, ['id'=>'total','class' => 'form-control item-total','readonly','required'=>'required','placeholder'=>'0.00'] ) !!}</td>
                            <td></td>
                        </tr>
                    @endif
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="6" align="left" class="text-left">
                            <a href="#" id="addRow" class="btn btn-sm btn-default" style="margin: 10px 0;"><i class="fa fa-plus-square"></i> ADD ITEM</a>

                        </th>
                    </tr>
                    </tfoot>
                </table>

                <div class="card-body">
                    <div class="d-md-flex flex-md-wrap">
                        <div class="pt-2 mb-3 wmin-md-400 ml-auto">
                            <h6 class="mb-3">Total due</h6>
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <th>Subtotal:</th>
                                        <td class="text-right"> <td>{!! Form::text('inv_total',NULL, ['id'=>'invTotal','class' => 'form-control ','readonly','required'=>'required','placeholder'=>'0.00'] ) !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Discount: <span class="font-weight-normal"></span></th>
                                        <td class="text-right"><td>{!! Form::text('inv_discount',NULL, ['id'=>'invDiscount','class' => 'form-control invDiscount','placeholder'=>'0.00'] ) !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Total:</th>
                                        <td class="text-right"><td>{!! Form::text('grandTotal',NULL, ['id'=>'grandTotal','class' => 'form-control','readonly','required'=>'required','placeholder'=>'0.00'] ) !!}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>


                        </div>
                    </div>
                </div>


        </div>
    </div>

</div>
    {!! Form::close() !!}


<script type="text/javascript">

    function update_total() {
        var total = 0;

        $('.item-total').each(function(i){
            price = $(this).val();
            if (!isNaN(price)) total += Number(price);
        });

        $('#invTotal').val(total.toFixed(3));
        $('#grandTotal').val(total.toFixed(3));
        update_balance();
    }

    // Update total balance
    function update_price() {
        var row = $(this).parents('.item-row');
        var price = row.find('.item-cost').val() * row.find('.item-qty').val();
        price = price.toFixed(3);
        isNaN(price) ? row.find('.item-total').val("N/A") : row.find('.item-total').val(price);
        update_total();
    }

    // Update total balance
    function update_balance() {
        var grandTotal = $("#invTotal").val().replace("$","") - $("#invDiscount").val();
        grandTotal =  grandTotal.toFixed(3);
        $('#grandTotal').val(grandTotal);
    }


    function bind() {
        $(".item-cost").blur(update_price);
        $(".item-qty").blur(update_price);
        $(".invDiscount").blur(update_price);
    }


        $(document).ready(function() {
            $('input').click(function(){
                $(this).select();
            });
            bind();

            $("#grandTotal").blur(update_balance);

        $("#addRow").click(function(){
            var counter = $("table#invoiceItems tbody tr.item-row").length;
            counter++;
            $(".item-row:last").after('<tr class="item-row"><td align="center" align="middle" style="vertical-align: middle;">'+counter+'</td><td><input type="hidden" name="invoiceItems['+counter+'][item_id]" value="'+counter+'"><input type="text" value="" Placeholder="Item Name" name="invoiceItems['+counter+'][item_title]" class="form-control" required></td><td><input type="textarea" name="invoiceItems['+counter+'][item_description]" value="" class="form-control" size="30x2" placeholder="Item Description" required="required"></td><td><input type="number" value="" Placeholder="0.00" name="invoiceItems['+counter+'][item_price]" class="form-control item-cost" required></td><td><input type="number" value="1" Placeholder="0.00" name="invoiceItems['+counter+'][item_qty]" class="form-control item-qty" required></td><td><input id="total" type="text" value="" Placeholder="0.00" name="invoiceItems['+counter+'][item_total]" class="form-control item-total" required></td><td align="center" style="vertical-align: middle;"><div id="deleteItem"><a href="#" id=""><i class="remove far fa-trash-alt"></i></a></div></td> </tr>');
            bind();
        });

        // Remove button functionality
        $(document).on('click', '#deleteItem .remove', function(){
            $(this).parents('.item-row').remove();
            update_total();
        });
    });



</script>
@endsection

