@extends('layouts.app')

@section('content')

@section('toolbar')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Dashboard</span> - {{ isset($page_title ) ? $page_title  : 'default' }}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
            <div class="header-elements d-none text-center text-md-left mb-3 mb-md-0">
                <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">

                    <div class="btn-group mr-2" role="group" aria-label="First group">

                            <a class=" btn-lg btn btn-outline-danger" href="/dashboard/payments/{{ $invoice->id }}/create">Enter Payment</a>

                    </div>
                    <div class="btn-group mr-2" role="group" aria-label="First group">
                        @if($invoice->inv_status ='1')
                            <a class=" btn-lg btn btn-light" href="/dashboard/invoices/{{ $invoice->id }}/edit"><i class="far fa-edit"></i></a>
                        @endif
                    </div>
                    <div class="btn-group mr-2" role="group" aria-label="Second group">
                        <a  class=" btn-lg btn btn-light" href="/dashboard/invoices/{{ $invoice->id }}/downloadPDF"><i class="far fa-file-pdf"></i></a>


                    </div>
                    <div class="btn-group" role="group" aria-label="Third group">
                        <a  class="btn btn-lg bg-primary" href="/dashboard/retainerInvoices/create"><i class="icon-paperplane ml-2"></i> Send Invoice</a>
                    </div>
                </div>



            </div>        </div>
    </div>
    <!-- /page header -->
@endsection
<div class="row justify-content-md-center">
    <div class="col-sm-9">

    @if($invoice->client->retainerInvoices  )

                 @foreach($invoice->client->retainerInvoices as $retainers)
                         @if($retainers->rt_status !== 2)
                            <div class="alert alert-primary" r ="alert">
                                <p> {{ $retainers->rt_amount }}  Retainers  Available  <a href="/dashboard/invoices/{{ $invoice->id }}/paymentApply/{{ $retainers->id }}">Apply to Invoice</a></p>
                            </div>
                         @endif
                 @endforeach

        @endif

        <div class="card">
            <div class="card-header bg-transparent header-elements-inline">
                <h6 class="card-title">Invoice ID #{{$invoice->inv_number}}</h6>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="mb-4">
                            <img src="{{asset('dist/global_assets/images/logo-company.png')}}" class="mb-3 mt-2" alt="" style="width: 245px;">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="mb-4">
                            <div class="text-sm-right">
                                <ul class="list list-unstyled mb-0">
                                    <li><span class="font-weight-semibold">Insibe Technologies Co.WLL | CR No.117003-1</span></li>
                                    <li><span class="font-weight-semibold">PO Box 3045 | Kingdom of Bahrain</span></li>
                                    <li><span class="font-weight-semibold">M. +973 35928671 | 37233959</span></li>
                                    <li><span class="font-weight-semibold">E. mail@insibe.com | W. www.insibe.com</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-md-flex flex-md-wrap">
                    <div class="mb-4 mb-md-2">
                        <span class="text-muted">Invoice To:</span>
                        <ul class="list list-unstyled mb-0">
                            @if ($invoice->client->primary_contact)
                                {!! $invoice->client->primary_contact!!} -
                            @endif
                            @if ($invoice->client->company )
                                <span>  {!! $invoice->client->company !!}</span><br>
                            @endif
                            @if ($invoice->client->address1 )
                                <span>  {!! $invoice->client->address1 !!}</span>,
                            @endif
                            @if ($invoice->client->address2 )
                                <span>  {!! $invoice->client->address2 !!}</span> <br>
                            @endif
                            @if ($invoice->client->address2 )
                                <span>  {!! $invoice->client->address2 !!}</span><br>
                            @endif
                            @if ($invoice->client->work_phone )
                                <span> P. {!! $invoice->client->work_phone  !!}</span> |
                            @endif
                            @if ($invoice->client->email )
                                <span> E. {!! $invoice->client->email  !!}</span> <br>
                            @endif
                        </ul>
                    </div>


                </div>
            </div>
            <div class="table-responsive">
                <table class="table ">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Items</th>
                        <th>Price</th>
                        <th>Qty	</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($invoice->invoiceItems as $item )
                        <tr class="item-row" align="left" style="vertical-align: middle;">
                            <td align="center" align="middle" style="vertical-align: middle;">1</td>
                            <td>{{$item->invoice_item}} <br> {{$item->invoice_itemDsc}}</td>
                            <td>{{$item->invoice_itemPrice}}</td>
                            <td>{{$item->invoice_itemQty}}</td>
                            <td>{{$item->invoice_itemTotal}}</td>
                        </tr>

                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="card-body">
                <div class="d-md-flex flex-md-wrap">
                    <div class="pt-2 mb-3">



                    </div>

                    <div class="pt-2 mb-3 wmin-md-400 ml-auto">
                        <h6 class="mb-3">Total due</h6>
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th>Subtotal:</th>
                                    <td class="text-right">{{ $invoice->inv_total }}</td>
                                </tr>
                                @if ($invoice->inv_discount !== null)
                                <tr>
                                    <th> Discount  </th>
                                    <td class="text-right">BHD @if ($invoice->inv_discount) {{$invoice->inv_discount}} @endif </td>
                                </tr>
                                @endif
                                <tr>
                                    <th>Total:</th>
                                    <td class="text-right">BHD {{ $invoice->grandTotal }}</td>
                                </tr>
                                @if ($invoice->amount_paid !== null)
                                <tr>
                                    <th> Amount Paid </th>
                                    <td class="text-right"> @if ($invoice->amount_paid)  BHD {{$invoice->amount_paid}} @endif </td>
                                </tr>
                                @endif
                                @if ($invoice->due_amount)
                                    <tr>
                                    <th>Balance Due:</th>
                                    <td class="text-right text-primary"><h5 class="font-weight-semibold">BHD {{ $invoice->due_amount }}</h5></td>
                                </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
 @endsection
