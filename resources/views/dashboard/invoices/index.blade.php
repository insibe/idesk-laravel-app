@extends('layouts.app')

@section('content')

@section('toolbar')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Dashboard</span> - {{ isset($page_title ) ? $page_title  : 'default' }}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none text-center text-md-left mb-3 mb-md-0">
                <div class="header-elements">
                    <a  class="btn btn-lg bg-primary" href="/dashboard/invoices/create">New Invoice</a>
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->
@endsection
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <table class="table table-hover">
                    <thead>
                    <tr >
                        <th>Status</th>
                        <th>Invoice ID</th>
                        <th>Invoice Date</th>
                        <th>Invoice Type</th>
                        <th>Client</th>
                        <th>Amount</th>
                        <th>Send Date</th>
                        <th>Options</th>
                    </tr>
                    </thead>

                    <tbody>
                    @if(count($invoices) > 0)
                        @foreach ($invoices as $invoice)
                            <tr>
                                <td> {{ $invoice->status() }}</td>
                                <td><a href="/dashboard/invoices/{{ $invoice->id }}">{{ $invoice->inv_number }} </a></td>
                                <td>{{ $invoice->inv_date }}   </td>
                                <td> {{ $invoice->type() }}</td>

                                <td><a href="/dashboard/clients/{{ $invoice->client->id }}">{{ $invoice->client->primary_contact}} </a>  </td>
                                <td> BHD {{ $invoice->grand_total}}   </td>
                                <td>{{ $invoice->balanceDue}}   </td>
                                <td>{{ $invoice->last_send_date}}   </td>



                            </tr>
                        @endforeach
                    @else
                        <tr><td colspan="5" class="text-info text-center">No items found.</td></tr>
                    @endif
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>


@endsection
