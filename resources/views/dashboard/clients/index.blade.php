@extends('layouts.app')

@section('content')
@section('toolbar')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Dashboard</span> - {{ isset($page_title ) ? $page_title  : 'default' }}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none text-center text-md-left mb-3 mb-md-0">
                <a  class="btn btn-lg bg-primary" href="/dashboard/clients/create">Add New Client</a>
            </div>
        </div>
    </div>
    <!-- /page header -->


@endsection

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                        <tr >
                            <th>Client Info</th>
                            <th >Primary Email</th>
                            <th>Primary Contacts</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>

                        @if(count($clients) > 0)
                            @foreach ($clients as $client)
                                <tr >
                                    <td><a href="/dashboard/clients/{{ $client->id }}">{{ $client->primary_contact }}</a>
                                        <small>{{ $client->company }}</small>
                                    </td>
                                    <td > {{ $client->email }}
                                    </td>
                                    <td > {{ $client->work_phone }}
                                        <small ></small>{{  $client->mobile }}</small>
                                    </td>
                                    <td ><span class="badge bg-blue">Active</span> </td>
                                    <td class="text-center">
                                        <div class="list-icons">
                                            <div class="list-icons-item dropdown">
                                                <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown" aria-expanded="false"><i class="icon-menu7"></i></a>
                                                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(16px, 16px, 0px);">
                                                    <a href="/dashboard/clients/{{ $client->id }}" class="dropdown-item"><i class="icon-file-stats"></i> View </a>
                                                    <a href="/dashboard/clients/{{ $client->id }}/edit" class="dropdown-item"><i class="icon-file-text2"></i> Edit Profile</a>
                                                    <a href="#" class="dropdown-item"><i class="icon-file-locked"></i> Create Invoice</a>
                                                    <a href="#" class="dropdown-item"><i class="icon-file-locked"></i> Create Proposal</a>
                                                    <div class="dropdown-divider"></div>
                                                    <a href="#" class="dropdown-item"><i class="icon-gear"></i> Delelte</a>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr><td colspan="5" class="text-info text-center">No items found.</td></tr>
                        @endif




                        </tbody>
                    </table>

                </div>
                </div>
        </div>
    </div>

@endsection
