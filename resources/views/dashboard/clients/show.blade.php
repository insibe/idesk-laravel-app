@extends('layouts.app')

@section('content')
@section('toolbar')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Dashboard</span> - {{ isset($page_title ) ? $page_title  : 'default' }}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <div class="header-elements d-none text-center text-md-left mb-3 mb-md-0">
                <a  class="btn btn-lg bg-primary" href="/dashboard/clients/create">Add New Client</a>
            </div>
        </div>
    </div>
    <!-- /page header -->


@endsection

<div class="row">
    <div class="col-sm-4">
        <div class="card">
            <div class="card-body text-center">
                <div class="card-img-actions d-inline-block mb-3">
                    <img class="img-fluid rounded-circle" src="../../../../global_assets/images/demo/users/face11.jpg" width="170" height="170" alt="">
                    <div class="card-img-actions-overlay card-img rounded-circle">
                        <a href="#" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round">
                            <i class="icon-plus3"></i>
                        </a>
                        <a href="user_pages_profile.html" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round ml-2">
                            <i class="icon-link"></i>
                        </a>
                    </div>
                </div>

                <h6 class="font-weight-semibold mb-0">Hanna Dorman</h6>
                <span class="d-block text-muted">UX/UI designer</span>

                <div class="list-icons list-icons-extended mt-3">
                    <a href="#" class="list-icons-item" data-popup="tooltip" title="" data-container="body" data-original-title="Google Drive"><i class="icon-google-drive"></i></a>
                    <a href="#" class="list-icons-item" data-popup="tooltip" title="" data-container="body" data-original-title="Twitter"><i class="icon-twitter"></i></a>
                    <a href="#" class="list-icons-item" data-popup="tooltip" title="" data-container="body" data-original-title="Github"><i class="icon-github"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection
