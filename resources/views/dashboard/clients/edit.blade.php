@extends('layouts.app')


@section('content')
@section('toolbar')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Dashboard</span> - {{ isset($page_title ) ? $page_title  : 'default' }}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
            {!! Form::model($clients, array( 'method' => $formMethod, 'url' => $url ,'data-parsley-validate','files' => 'true','no-validate', 'enctype'=>'multipart/form-data')) !!}
            <div class="header-elements d-none text-center text-md-left mb-3 mb-md-0">
                <button type="submit" class="btn btn-lg btn-primary">Save Client </button>
            </div>
        </div>
    </div>
    <!-- /page header -->
@endsection

<div class="card">

    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-form-label">Primary Contact</label>
                    {!! Form::text('primary_contact',NULL, ['class' => 'form-control','required' =>'required'] ) !!}
                </div>
                <div class="form-group">
                    <label class="col-form-label">Company Name</label>
                    {!! Form::text('company',NULL, ['class' => 'form-control'] ) !!}
                </div>
                <div class="form-group">
                    <label class="col-form-label">Primary Email</label>
                    {!! Form::email('email',NULL, ['class' => 'form-control'] ) !!}

                </div>
                <div class="form-group">
                    <label class="col-form-label">Work Phone</label>
                    {!! Form::text('work_phone',NULL, ['class' => 'form-control','data-parsley-type'=>'number'] ) !!}

                </div>
                <div class="form-group">
                    <label class="col-form-label">Mobile</label>
                    {!! Form::text('mobile_phone',NULL, ['class' => 'form-control','data-parsley-type'=>'number'] ) !!}

                </div>
                <div class="form-group">
                    <label class="col-form-label">Website</label>
                    {!! Form::text('website',NULL, ['class' => 'form-control'] ) !!}

                </div>

            </div>
            <!-- /.co l -->
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-form-label">Street </label>
                    {!! Form::text('address1',NULL, ['class' => 'form-control'] ) !!}
                </div>
                <div class="form-group">
                    <label class="col-form-label">Apt/Suite </label>
                    {!! Form::text('address2',NULL, ['class' => 'form-control'] ) !!}
                </div>
                <div class="form-group">
                    <label class="col-form-label">City</label>
                    {!! Form::text('city',NULL, ['class' => 'form-control'] ) !!}
                </div>
                <div class="form-group">
                    <label class="col-form-label">State/Province </label>
                    {!! Form::text('state',NULL, ['class' => 'form-control'] ) !!}
                </div>
                <div class="form-group">
                    <label class="col-form-label">PO Box </label>
                    {!! Form::number('postal_code',NULL, ['class' => 'form-control','data-parsley-type'=>'number'] ) !!}
                </div>
                <div class="form-group">
                    <label class="col-form-label">Country </label>
                    {!! Form::select('country_id', ['Bahrain', 'india'], null, ['class' => 'form-control','required' =>'required']) !!}
                </div>
            </div>
            <!-- /.col -->
        </div>

    </div>
</div>

{!! Form::close() !!}
@endsection
