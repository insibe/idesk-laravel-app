@extends('layouts.dashboard')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ isset($page_title ) ? $page_title  : 'default' }}

            </h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="breadcrumb-item active">Here</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">



        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
 

 
        <div class="row">

        <div class="center-content">

  <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary ">
                <div class="box-header with-border">
                  <h3 class="box-title">   {{ isset($page_title ) ? $page_title  : 'default' }}</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {{-- {{ print_r($retailers) }} --}}


                  <div class="box-body">

                      <div class="form-group col-sm-12">
                          <label>Category Title </label>
                          {!! Form::text('title',NULL, ['class' => 'form-control','required' =>'required'] ) !!}
                      </div>

                      <div class="form-group col-sm-12">
                          <label>Category Icon </label>
                          {!! Form::text('icon',NULL, ['class' => 'form-control','required' =>'required'] ) !!}
                      </div>

                      <div class="form-group col-sm-12">
                          <label>Category Description </label>
                          {!! Form::textarea('description',NULL, ['class' => 'form-control','size' => '30x2']) !!}
                      </div>
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                      <button type="submit" class="btn btn-sm btn-danger  pull-right">Save</button>
                  </div><!-- /.box-footer -->
              </div><!-- /.box -->


            </div><!--/.col (left) -->


        </div>


          </div>
        {!! Form::close() !!}

       


  



@endsection
@section('footer.scripts')

<script type="text/javascript">
  $('#form').parsley();
</script>

  
@stop
