@extends('layouts.app')

@section('content')
@section('toolbar')
    <!-- Page header -->
@section('toolbar')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Dashboard</span> - {{ isset($page_title ) ? $page_title  : 'default' }}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>


            <div class="header-elements d-none text-center text-md-left mb-3 mb-md-0">
                <div class="btn-group mr-2" role="group" aria-label="Third group">
                    <a  class="btn btn-sm btn-default" href="/dashboard/proposal/downloadTemplate"><i class="fas fa-download"></i> Download Template </a>
                </div>
                <div class="btn-group" role="group" aria-label="Third group">
                    <a  class="btn btn-lg bg-primary" href="/dashboard/proposals/create">Create New Proposal</a>
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->
@endsection
    <!-- /page header -->
@endsection
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                        <tr >
                            <th>Status</th>
                            <th>Proposal Info</th>
                            <th>Proposal Date</th>
                            <th>Client</th>
                            <th>Proposal Amount</th>
                        </tr>
                        </thead>

                        <tbody>
                        @if(count($proposals) > 0)
                            @foreach ($proposals as $proposal)
                                <tr>
                                    <td> {{ $proposal->status() }}</td>
                                    <td> <a href="/dashboard/proposals/{{ $proposal->id }}">{{ $proposal->pro_number}} <small> {{ $proposal->pro_title}} </small></a> </td>
                                    <td> {{ $proposal->pro_date}}</td>
                                    <td>
                                        <a href="/dashboard/clients/{{ $proposal->client->id }}">{{ $proposal->client->primary_contact}}</a>

                                       </td>
                                    <td> {{ $proposal->pro_grandTotal}}</td>

                                </tr>
                            @endforeach
                        @else
                            <tr><td colspan="5" class="text-info text-center">No items found.</td></tr>
                        @endif
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>

@endsection
