@extends('layouts.app')

@section('header-scripts')

    <script src="{{asset('dist/global_assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script src="{{asset('dist/global_assets/js/plugins/uploaders/dropzone.min.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.select-search').select2();
            $( "#datepicker" ).datepicker({ dateFormat: 'dd MM, yy' });
        });
    </script>


@endsection
@section('content')
@section('toolbar')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Dashboard</span> - {{ isset($page_title ) ? $page_title  : 'default' }}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            {!! Form::model($proposal, array( 'method' => $formMethod, 'url' => $url, 'class'=>'horizontal-form  ' ,'data-parsley-validate','files' => 'true','no-validate', 'enctype'=>'multipart/form-data')) !!}

            <div class="header-elements d-none text-center text-md-left mb-3 mb-md-0">
                <button type="submit" class="btn btn-lg bg-primary">Create Proposal </button>
            </div>
        </div>
    </div>
    <!-- /page header -->
@endsection
<div class="row justify-content-md-center">
    <div class="col-sm-8">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label class="col-form-label">Client</label>
                            {!! Form::select('client_id',$clients, null, ['class' => 'form-control select-search','required' =>'required']) !!}
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label class="col-form-label">Proposal Date </label>
                            {!! Form::text('pro_date', isset($proposal->pro_date ) ? $proposal->pro_date  : $pro_date  , ['id'=>'datepicker','class' => 'form-control pickadate','required' =>'required']) !!}
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label class="col-form-label">Proposal# </label>
                            {!! Form::text('pro_number',isset($proposal->pro_number ) ? $proposal->pro_number  : $pro_number  , ['readonly','class' => 'form-control','required' =>'required']) !!}
                        </div>
                    </div>



                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <div class="form-group">
                                <label class="col-form-label">Upload Proposal</label>
                                <div class="dropzone-previews"></div>

                            </div>
                        </div>

                    </div>
                    <div class="col-3">

                        <div class="form-group">
                            <label class="col-form-label">Proposal Grand Total </label>
                            {!! Form::text('pro_grandTotal',null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-3">

                        <div class="form-group">
                            <label class="col-form-label">Account Manager </label>
                            {!! Form::select('acntManager_id',$clients, null, ['class' => 'form-control select-search','required' =>'required']) !!}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
{!! Form::close() !!}

<script type="text/javascript">

    Dropzone.options.dropzoneSingle = {
        paramName: "file", // The name that will be used to transfer the file
        maxFilesize: 20, // MB
        maxFiles: 5,
        dictDefaultMessage: 'Drop file to upload <span>or CLICK</span>',
        acceptedFiles: ".pdf",
        autoProcessQueue: true,
        init: function() {
            this.on('addedfile', function(file){
                if (this.fileTracker) {
                    this.removeFile(this.fileTracker);
                }
                this.fileTracker = file;
            });
        }
    };




</script>

@endsection
