@extends('layouts.app')

@section('content')
@section('toolbar')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Dashboard</span> - {{ isset($page_title ) ? $page_title  : 'default' }}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            {!! Form::model($proposal, array( 'method' => $formMethod, 'url' => $url, 'class'=>'horizontal-form  ' ,'data-parsley-validate','files' => 'true','no-validate', 'enctype'=>'multipart/form-data')) !!}

            <div class="header-elements d-none text-center text-md-left mb-3 mb-md-0">
                <button type="submit" class="btn btn-lg bg-primary">Generate Proposal </button>
            </div>
        </div>
    </div>
    <!-- /page header -->
@endsection
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-8">
                        <div class="form-group">
                            <label class="col-form-label">Proposal Title </label>
                            {!! Form::text('pro_title', null, ['class' => 'form-control','required' =>'required']) !!}
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Client</label>
                            {!! Form::select('client_id',$clients, null, ['class' => 'form-control selectbox','required' =>'required']) !!}
                        </div>

                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label class="col-form-label">Proposal# </label>
                            {!! Form::text('pro_number',isset($proposal->pro_number ) ? $proposal->pro_number  : $pro_number  , ['readonly','class' => 'form-control','required' =>'required']) !!}
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Account Manager </label>
                            {!! Form::text('pro_number',null, ['class' => 'form-control']) !!}
                        </div>
                    </div>


                    <div class="col-12">

                        <div class="form-group ">
                            <label class="col-form-label">Client</label>
                            {!! Form::select('client_id',$clients, null, ['class' => 'form-control selectcard','required' =>'required']) !!}
                        </div>
                        <div class="form-group ">
                            <label class="col-form-label">Account Manager</label>
                            {!! Form::select('acnt_manager',$clients, null, ['class' => 'form-control selectcard','required' =>'required']) !!}
                        </div>
                        <div class="form-group ">
                        <label class="col-form-label">Category</label>
                            {!! Form::select('pro_category',$clients, null, ['class' => 'form-control selectcard','required' =>'required']) !!}
                         </div>
                    </div>


                </div>
            </div>

        </div>
    </div>
    <div class="col-sm-4">
        <div class="card">

            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label class="col-form-label">Proposal # </label>
                            {!! Form::text('pro_number', isset($proposal->pro_number ) ? $proposal->pro_number: $pro_number , ['class' => 'form-control' ,'readonly']) !!}
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Proposal # </label>
                            {!! Form::text('pro_date',null, ['class' => 'form-control']) !!}
                        </div>

                        {{--<div class="form-group">--}}
                        {{--<label class="col-form-label">Status </label>--}}
                        {{--{!! Form::select('invoice_status', ['Draft', 'Sent','Approved','Rejected','Cancelled'], null, ['class' => 'form-control selectbox','required' =>'required']) !!}--}}
                        {{--</div>--}}

                        <div class="row">
                            <div class="col-12 text-center">
                                <div class="btn-group r" role="group" aria-label="Basic example">
                                    <button type="button" class="btn btn-outline-primary btn-lg">Preview</button>
                                    <button type="button" class="btn btn-outline-primary btn-lg">Download</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="card-footer">
                <div class="text-center">
                    <button type="submit" class="btn btn-primary btn-labeled btn-block btn-lg btn-labeled-center"><i class="icon-paperplane ml-2"></i> Save Invoice</button>
                </div>

            </div>

        </div>
    </div>
</div>
{!! Form::close() !!}
@endsection
