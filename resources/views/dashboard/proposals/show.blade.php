@extends('layouts.app')

@section('header-scripts')
    <link rel="stylesheet" href="{{ asset('dist/css/fileinput.css')}}">
    <script src="{{asset('dist/global_assets/js/plugins/forms/selects/select2.min.js')}}"></script>

    <script src="{{asset('dist/global_assets/js/plugins/uploaders/fileinput/plugins/purify.min.js')}}"></script>
    <script src="{{asset('dist/global_assets/js/plugins/uploaders/fileinput/plugins/sortable.min.js')}}"></script>
    <script src="{{asset('dist/global_assets/js/plugins/uploaders/fileinput/fileinput.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.form-control-select2').select2({
                width: 150,
                minimumResultsForSearch: Infinity
            });
            var btns = '<button type="button" data-toggle="modal" data-target="#modal_default" class="kv-cust-btn btn btn-kv" title="Edit"{dataKey}>' +
                '<i class="far fa-check-circle text-success"></i>' +
                '</button>';

            $(".file-input-ajax").fileinput({
                browseClass: "btn btn-default btn-block",
                uploadUrl: "/dashboard/proposal/{{ $proposal->id }}/storeAttachment",
                uploadAsync: false,
                showUpload: false, // hide upload button
                showCaption: false,
                showRemove: false,
                minFileCount: 1,
                maxFileCount: 5,
                overwriteInitial: false,
                initialPreview: {{$proposal->attachedProposals()}},
                previewFileIcon: '<i class="fa fa-file"></i>',
                initialPreviewAsData: true, // defaults markup
                initialPreviewFileType: 'image', // image is the default and can be overridden in config below
                initialPreviewConfig: {{ $proposal->attachmentsPreviewConfig() }},
                allowedFileExtensions: ["pdf", "doc","docx", "pptx", "ppt"],
                uploadExtraData: function() {
                    return {
                        _token: $("input[name='_token']").val()
                    };
                },
                preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
                previewFileIconSettings: { // configure your icon file extensions
                    'doc': '<i class="far fa-file-word text-primary"></i>',
                    'xls': '<i class="far fa-file-excel text-success"></i>',
                    'ppt': '<i class="far fa-file-powerpoint text-danger"></i>',
                    'pdf': '<i class="far fa-file-pdf text-danger"></i>'

                },
                previewFileExtSettings: { // configure the logic for determining icon file extensions
                    'doc': function(ext) {
                        return ext.match(/(doc|docx)$/i);
                    },
                    'xls': function(ext) {
                        return ext.match(/(xls|xlsx)$/i);
                    },
                    'ppt': function(ext) {
                        return ext.match(/(ppt|pptx)$/i);
                    },
                    'pdf': function(ext) {
                        return ext.match(/(pdf)$/i);
                    }

                },
                otherActionButtons: btns,
                initialPreviewDownloadUrl: 'https://picsum.photos/800/460?image={key}', // includes the dynamic key tag to be replaced for each config
                fileActionSettings: {
                    removeIcon: '<i class="icon-bin"></i>',
                    uploadIcon: '<i class="icon-upload"></i>',
                    downloadIcon: '<i class="icon-download"></i>',
                    zoomIcon: '<i class="icon-zoomin3"></i>',
                    indicatorNew: '<i class="icon-file-plus text-success"></i>',
                    indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
                    indicatorError: '<i class="icon-cross2 text-danger"></i>',
                    indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>'
                },

            }).on("filebatchselected", function(event, files) {
                $(".file-input-ajax").fileinput("upload");
            });


        });
    </script>

<style type="text/css">
    .proposal-accepted{
        border: 3px solid #4caf50 !important;
        background: #f8f8f8;
        }
</style>



@endsection
@section('content')
    @section('toolbar')





        <!-- Page header -->
        <div class="page-header">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Dashboard</span> - {{ isset($page_title ) ? $page_title  : 'default' }}</h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
                <div class="header-elements d-none text-center text-md-left mb-3 mb-md-0">

                    <div class="btn-group mr-2" role="group" aria-label="Third group">
                        <button type="button" class="btn btn-lg bg-primary" data-toggle="modal" data-target="#modal_iconified"><i class="fas fa-upload"></i> Upload Proposal</button>

                    </div>

                    <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">

                        <div class="btn-group" role="group" aria-label="Third group">
                            <a  class="btn btn-lg bg-primary" href="/dashboard/retainerInvoices/create"><i class="icon-paperplane ml-2"></i> Send Invoice</a>
                        </div>
                    </div>



                </div>        </div>
        </div>
        <!-- /page header -->
    @endsection
<div class="row">
    <div class="col-sm-8">
        <div class="card">
            <div class="card-body">
                <div class="row">
                        <div class="col-lg-12">
                            {!! csrf_field() !!}
                            <input type="file" name="file" class="file-input-ajax" multiple="multiple" data-fouc>
                            <span class="form-text text-muted">This scenario uses asynchronous/parallel uploads. Uploading itself is turned off in live preview.</span>
                        </div>
                </div>




            </div>
        </div>

    </div>
    <div class="col-sm-4">
        <div class="card border-left-4 border-left-success rounded-left-0">
            <div class="card-body">
                <div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
                    <div>
                        <h6 class="font-weight-semibold">{{ $proposal->client->primary_contact }}</h6>
                        <ul class="list list-unstyled mb-0">
                            <li>Proposal #: <a href="#">{{ $proposal->pro_number }}</a></li>
                            <li>Issued on: <span class="font-weight-semibold">{{ $proposal->pro_date }}</span></li>
                            <li>Prepared By: <span class="font-weight-semibold">SWIFT</span></li>
                        </ul>
                    </div>

                    <div class="text-sm-right mb-0 mt-3 mt-sm-0 ml-auto">
                        <ul class="list list-unstyled mb-0">

                            <li class="dropdown">
                                Status: &nbsp;
                                <a href="#" class="badge bg-success-400 align-top dropdown-toggle" data-toggle="dropdown">Accepted</a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="#" class="dropdown-item"><i class="icon-alert"></i> Rejected</a>
                                    <a href="#" class="dropdown-item"><i class="icon-alarm"></i> In Discussion</a>
                                    <a href="#" class="dropdown-item active"><i class="icon-checkmark3"></i> Paid</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="#" class="dropdown-item"><i class="icon-spinner2 spinner"></i> On hold</a>
                                    <a href="#" class="dropdown-item"><i class="icon-cross2"></i> Canceled</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="card-footer d-sm-flex justify-content-sm-between align-items-sm-center">
										<span>
											<span class="badge badge-mark border-success mr-2"></span>
											Proposal Cost:
											<span class="font-weight-semibold">
                                                @if ($proposal->pro_grandTotal)
                                                    {!! $proposal->pro_grandTotal!!}
                                                     @else
                                                     Not Set
                                                @endif
                                               </span>
										</span>

                <ul class="list-inline list-inline-condensed mb-0 mt-2 mt-sm-0">

                    <li class="list-inline-item dropdown">
                        <a href="#" class="text-default dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>

                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="/dashboard/proposals/{{ $proposal->id }}/edit" class="dropdown-item"><i class="icon-file-plus"></i> Edit Proposal</a>
                            <a href="#" class="dropdown-item"><i class="icon-cross2"></i> Delete Proposal</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

    </div>
</div>
<!-- Basic modal -->
<div id="modal_default" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Basic modal</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <h6 class="font-weight-semibold">Text in a modal</h6>
                <div class="form-group">
                    <label class="col-form-label">Select Client</label>

                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                <button type="button" class="btn bg-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- /basic modal -->

 @endsection
