@extends('layouts.app')

@section('header-scripts')

    <script src="{{asset('dist/global_assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script src="{{asset('dist/global_assets/js/plugins/uploaders/dropzone.min.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.select-search').select2();
            $( "#datepicker" ).datepicker({ dateFormat: 'dd MM, yy' });
        });
    </script>



@endsection
@section('content')
@section('toolbar')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Dashboard</span> - {{ isset($page_title ) ? $page_title  : 'default' }}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            {!! Form::model($proposal, array( 'method' => $formMethod, 'url' => $url, 'id'=>'dropzoneUpload','class'=>'horizontal-form ' ,'data-parsley-validate','files' => 'true','no-validate', 'enctype'=>'multipart/form-data')) !!}

            <div class="header-elements d-none text-center text-md-left mb-3 mb-md-0">
                <button type="submit" class="btn btn-lg bg-primary">Create Proposal </button>
            </div>
        </div>
    </div>
    <!-- /page header -->
@endsection
<div class="row justify-content-md-center">
    <div class="col-sm-6">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">

                        <div class="form-group">
                            <label class="col-form-label">Proposal # </label>
                            {!! Form::text('pro_number',isset($invoice->pro_number ) ? $invoice->pro_number  : $pro_number  , ['readonly','class' => 'form-control','required' =>'required']) !!}
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Proposal Title</label>

                            {!! Form::text('pro_title', null, ['class' => 'form-control','required' =>'required']) !!}
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Select Client</label>
                            {!! Form::select('client_id',$clients, null, ['class' => 'form-control select-search','required' =>'required']) !!}
                        </div>
                        <div class="form-group">
                                <label class="col-form-label">Account Manager </label>
                                {!! Form::select('acntManager_id',$clients, null, ['class' => 'form-control select-search','required' =>'required']) !!}
                            </div>

                    </div>

                </div>
                <div class="row">
                    <div class="col-3">

                        <div class="form-group">
                            <label class="col-form-label">Proposal Grand Total </label>
                            {!! Form::text('pro_grandTotal',null, ['class' => 'form-control']) !!}
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

</div>
{!! Form::close() !!}




@endsection
