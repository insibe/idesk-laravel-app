@extends('layouts.app')

@section('header-scripts')

    <script src="{{asset('dist/global_assets/js/plugins/forms/selects/select2.min.js')}}"></script>

    <script src="{{asset('dist/global_assets/js/plugins/uploaders/fileinput/plugins/purify.min.js')}}"></script>
    <script src="{{asset('dist/global_assets/js/plugins/uploaders/fileinput/plugins/sortable.min.js')}}"></script>
    <script src="{{asset('dist/global_assets/js/plugins/uploaders/fileinput/fileinput.min.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.form-control-select2').select2({
                width: 150,
                minimumResultsForSearch: Infinity
            });


            // Modal template
            var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
                '  <div class="modal-content">\n' +
                '    <div class="modal-header align-items-center">\n' +
                '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
                '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
                '    </div>\n' +
                '    <div class="modal-body">\n' +
                '      <div class="floating-buttons btn-group"></div>\n' +
                '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
                '    </div>\n' +
                '  </div>\n' +
                '</div>\n';

            // Buttons inside zoom modal
            var previewZoomButtonClasses = {
                toggleheader: 'btn btn-light btn-icon btn-header-toggle btn-sm',
                fullscreen: 'btn btn-light btn-icon btn-sm',
                borderless: 'btn btn-light btn-icon btn-sm',
                close: 'btn btn-light btn-icon btn-sm'
            };

            // Icons inside zoom modal classes
            var previewZoomButtonIcons = {
                prev: '<i class="icon-arrow-left32"></i>',
                next: '<i class="icon-arrow-right32"></i>',
                toggleheader: '<i class="icon-menu-open"></i>',
                fullscreen: '<i class="icon-screen-full"></i>',
                borderless: '<i class="icon-alignment-unalign"></i>',
                close: '<i class="icon-cross2 font-size-base"></i>'
            };

            // File actions
            var fileActionSettings = {
                zoomClass: '',
                zoomIcon: '<i class="icon-zoomin3"></i>',
                dragClass: 'p-2',
                dragIcon: '<i class="icon-three-bars"></i>',
                removeClass: '',
                removeErrorClass: 'text-danger',
                removeIcon: '<i class="icon-bin"></i>',
                indicatorNew: '<i class="icon-file-plus text-success"></i>',
                indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
                indicatorError: '<i class="icon-cross2 text-danger"></i>',
                indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>'
            };

            $('.file-input-ajax').fileinput({
                browseLabel: 'Browse',
                uploadUrl: "/dashboard/proposal/{{ $proposal->id }}/storeAttachment", // server upload action
                uploadAsync: false,
                showUpload: false, // hide upload button
                showRemove: false, // hide remove button
                maxFileCount: 5,
                minImageWidth: 50,
                minImageHeight: 50,
                reversePreviewOrder: true,
                overwriteInitial: false,
                initialPreview: {{$proposal->attachedProposals()}},
                initialPreviewAsData: true, // defaults markup
                initialPreviewFileType: 'image', // image is the default and can be overridden in config below
                initialPreviewConfig: [
                    {type: "pdf", size: 8000, caption: "PDF Sample", filename: "PDF-Sample.pdf", url: "/file-upload-batch/2", key: 14},
                    {type: "pdf", size: 8000, caption: "PDF Sample", filename: "PDF-Sample.pdf", url: "/file-upload-batch/2", key: 14},
                    {type: "pdf", size: 8000, caption: "PDF Sample", filename: "PDF-Sample.pdf", url: "/file-upload-batch/2", key: 14},
                    {type: "pdf", size: 8000, caption: "PDF Sample", filename: "PDF-Sample.pdf", url: "/file-upload-batch/2", key: 14}

                ],
                initialPreviewThumbTags:[
                    {
                        '{CUSTOM_TAG_INIT}': '<span class=\'custom-css\'>CUSTOM MARKUP</span>'
                    }
                ],
                preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
                showBrowse: true,
                browseOnZoneClick: false,
                uploadExtraData: function() {
                    return {
                        _token: $("input[name='_token']").val()
                    };
                },
                browseIcon: '<i class="icon-file-plus mr-2"></i>',
                uploadIcon: '<i class="icon-file-upload2 mr-2"></i>',
                removeIcon: '<i class="icon-cross2 font-size-base mr-2"></i>',
                fileActionSettings: {
                    removeIcon: '<i class="icon-bin"></i>',
                    uploadIcon: '<i class="icon-upload"></i>',
                    uploadClass: '',
                    zoomIcon: '<i class="icon-zoomin3"></i>',
                    zoomClass: '',
                    indicatorNew: '<i class="icon-file-plus text-success"></i>',
                    indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
                    indicatorError: '<i class="icon-cross2 text-danger"></i>',
                    indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>',
                },
                layoutTemplates: {
                    icon: '<i class="icon-file-check"></i>',
                    modal: modalTemplate
                },
                initialCaption: 'No file selected',
                previewZoomButtonClasses: previewZoomButtonClasses,
                previewZoomButtonIcons: previewZoomButtonIcons
            }).on("filebatchselected", function(event, files) {
                $(".file-input-ajax").fileinput("upload");
            });

        });
    </script>



<style type="text/css">

</style>

@endsection
@section('content')
    @section('toolbar')





        <!-- Page header -->
        <div class="page-header">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Dashboard</span> - {{ isset($page_title ) ? $page_title  : 'default' }}</h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
                <div class="header-elements d-none text-center text-md-left mb-3 mb-md-0">

                    <div class="btn-group mr-2" role="group" aria-label="Third group">
                        <button type="button" class="btn btn-lg bg-primary" data-toggle="modal" data-target="#modal_iconified"><i class="fas fa-upload"></i> Upload Proposal</button>

                    </div>

                    <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">

                        <div class="btn-group" role="group" aria-label="Third group">
                            <a  class="btn btn-lg bg-primary" href="/dashboard/retainerInvoices/create"><i class="icon-paperplane ml-2"></i> Send Invoice</a>
                        </div>
                    </div>



                </div>        </div>
        </div>
        <!-- /page header -->
    @endsection
<div class="row">
    <div class="col-sm-8">
        <div class="card">
            <div class="card-body">
                <div class="row">
                        <div class="col-lg-12">
                            {!! csrf_field() !!}
                            <input type="file" name="file" class="file-input-ajax" multiple="multiple" data-fouc>
                            <span class="form-text text-muted">This scenario uses asynchronous/parallel uploads. Uploading itself is turned off in live preview.</span>
                        </div>
                </div>




            </div>
        </div>

    </div>
    <div class="col-sm-4">
        <div class="card border-left-4 border-left-success rounded-left-0">
            <div class="card-body">
                <div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
                    <div>
                        <h6 class="font-weight-semibold">{{ $proposal->client->primary_contact }}</h6>
                        <ul class="list list-unstyled mb-0">
                            <li>Proposal #: <a href="#">{{ $proposal->pro_number }}</a></li>
                            <li>Issued on: <span class="font-weight-semibold">{{ $proposal->pro_date }}</span></li>
                            <li>Prepared By: <span class="font-weight-semibold">SWIFT</span></li>
                        </ul>
                    </div>

                    <div class="text-sm-right mb-0 mt-3 mt-sm-0 ml-auto">
                        <ul class="list list-unstyled mb-0">

                            <li class="dropdown">
                                Status: &nbsp;
                                <a href="#" class="badge bg-success-400 align-top dropdown-toggle" data-toggle="dropdown">Accepted</a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="#" class="dropdown-item"><i class="icon-alert"></i> Rejected</a>
                                    <a href="#" class="dropdown-item"><i class="icon-alarm"></i> In Discussion</a>
                                    <a href="#" class="dropdown-item active"><i class="icon-checkmark3"></i> Paid</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="#" class="dropdown-item"><i class="icon-spinner2 spinner"></i> On hold</a>
                                    <a href="#" class="dropdown-item"><i class="icon-cross2"></i> Canceled</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="card-footer d-sm-flex justify-content-sm-between align-items-sm-center">
										<span>
											<span class="badge badge-mark border-success mr-2"></span>
											Proposal Cost:
											<span class="font-weight-semibold">
                                                @if ($proposal->pro_grandTotal)
                                                    {!! $proposal->pro_grandTotal!!}
                                                     @else
                                                     Not Set
                                                @endif
                                               </span>
										</span>

                <ul class="list-inline list-inline-condensed mb-0 mt-2 mt-sm-0">

                    <li class="list-inline-item dropdown">
                        <a href="#" class="text-default dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>

                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="/dashboard/proposals/{{ $proposal->id }}/edit" class="dropdown-item"><i class="icon-file-plus"></i> Edit Proposal</a>
                            <a href="#" class="dropdown-item"><i class="icon-cross2"></i> Delete Proposal</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

    </div>
</div>

 @endsection
