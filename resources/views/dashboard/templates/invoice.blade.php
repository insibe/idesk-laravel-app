
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Insibe-{{$invoice->inv_number}}</title>
    <style type="text/css">
        @page{margin: 50px 0}
        body{font-family: 'Inter UI', sans-serif; font-size: 12px; line-height: 1.7em;}
        .inv_wrapper{width: 80%; margin: 0 auto}
        #inv_logo{display: inline-block; float:left; width: 50%; vertical-align: middle}
        #inv_address{float:right;width: 50%; text-align: right;}
        #inv_details{background: #f8f8f8; padding: 30px 0;}
        #inv_detialsLft{display: inline-block; float:left; width: 50%; vertical-align: middle}
        #inv_detialsRgt{display: inline-block; float:right; width: 50%; vertical-align: middle}
        #pay_info{padding: 10px;border: 1px solid #f8f8f8; float: left; width: 50%; s;}
        #inv_info{padding: 10px;border: 1px solid #f2f2f2; float: right; width: 50%; background: #f8f8f8;}

        #inv_table { width:100%; margin:30px 0; font-size: 15px; }
        #inv_table th { font-weight:600; }
        #inv_table thead th { background-color:#D9D9D9; font-size:14px; text-align:center; vertical-align:middle; border-right:dashed 1px #fff; }
        #inv_table thead th:last-child { border-right:none; }
        #inv_table tbody td { border-bottom:dashed 1px #d9d9d9; border-right:dashed 1px #d9d9d9; vertical-align:middle; }
        #inv_table tbody td:last-child { border-right:none; }
        #inv_table tbody tr:nth-child(even) { background-color:#FAFAFA; }
        #inv_table tbody tr:last-child td { border-bottom:dashed 1px #BBB; }
        #inv_table tbody td:nth-child(1) { text-align:center; }
        #inv_table tbody td:nth-child(2) { text-align:left; }
        #inv_table tbody td:nth-child(3) { text-align:right; }
        #inv_table tbody td:nth-child(4) { text-align:right; }
        #inv_table tbody td:nth-child(5) { text-align:right; }
        #inv_table tfoot th { vertical-align:middle; text-align:right; }
        #inv_table tfoot th:last-child { border-top:none; border-bottom:solid 1px #d9d9d9; background-color:#fafafa; }
        #inv_table tfoot tr:first-child th:last-child { background-color:#fafafa; }
        #inv_table tfoot tr:last-child th:last-child { border-bottom:double 3px #BBB; background-color:#eee; font-size:16px; }
        #inv_table tfoot th.weak { font-weight:300; text-align:left; vertical-align:bottom; }

    </style>
</head>
<body>
<div id="inv_header">
    <div class="inv_wrapper">
        <div id="inv_logo">
            <img width="200" src="http://insibe.net/insibe-logo.png">
        </div>
        <div id="inv_address"><!-- LLPIN Reg No: L01094531<br> -->
            Insibe Technologies Co.WLL | CR No.117003-1<br>
            PO Box 3045 | Kingdom of Bahrain<br>
            M. +973 35928671 , 37233959<br>
            E. mail@insibe.com | W. www.insibe.com
        </div>
    </div>
</div><!-- #inv_invoice -->
<div id="inv_details">
    <div class="inv_wrapper">
        <div id="inv_detialsLft">
            <div class="title">INVOICE # {{$invoice->inv_number}} </div>
            <div class="inv_detail_line "><span>Invoice Date</span><span>:</span><span class="inv_date"> {{$invoice->inv_date}} </span></div>
            <div class="inv_detail_line "><span>Issue Date</span><span>:</span><span class="inv_date"> {{$invoice->inv_date}} </span></div>
        </div>
        <div id="inv_detialsRgt">
            <div class="title">ISSUED TO</div>
            @if ($invoice->client->primary_contact)
                {!! $invoice->client->primary_contact!!} -
            @endif
            @if ($invoice->client->company )
                <span>  {!! $invoice->client->company !!}</span><br>
            @endif
            @if ($invoice->client->address1 )
                <span>  {!! $invoice->client->address1 !!}</span>,
            @endif
            @if ($invoice->client->address2 )
                <span>  {!! $invoice->client->address2 !!}</span> <br>
            @endif

            @if ($invoice->client->work_phone )
                <span> P. {!! $invoice->client->work_phone  !!}</span> |
            @endif
            @if ($invoice->client->email )
                <span> E. {!! $invoice->client->email  !!}</span> <br>
            @endif
        </div>
    </div>
</div>
<div id="inv_items">
    <div class="inv_wrapper">
        <table id="inv_table" cellpadding="8" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>No</th>
                <th>Items</th>
                <th>Price</th>
                <th>Qty</th>
                <th>Total</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($invoice->invoiceItems as $item )
            <tr>
                <td width="10" align="center" style="vertical-align: middle;">1</td>
                <td>{{$item->invoice_item}} <br>{{$item->invoice_itemDsc}}</td>
                <td>{{$item->invoice_itemPrice}}</td>
                <td>{{$item->invoice_itemQty}}</td>
                <td>{{$item->invoice_itemTotal}}</td>
            </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th colspan="2" rowspan="5" class="weak">Amount in words: <span id="inv_amount_words"><?php $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                        echo ucwords($f->format($invoice->due_amount)); ?></span> only</th>
                <th colspan="2">Sub Total</th>
                <th>{{ $invoice->inv_total }}</th>
            </tr>
            <tr>
                    <th colspan="2">  @if ($invoice->inv_discount) Discount @endif</th>
                    <th>@if ($invoice->inv_discount) {{$invoice->inv_discount}} @endif</th>
            </tr>
            <tr>
                <th colspan="2">Total</th>
                <th>BHD {{ $invoice->grandTotal }}</th>
            </tr>
            <tr>
                <th colspan="2">  @if ($invoice->amount_paid) Amount Paid @endif</th>
                <th>@if ($invoice->amount_paid) BHD {{$invoice->amount_paid}} @endif</th>
            </tr>
            <tr style="font-size: 25px; background-color: #f1f1f1; font-weight: 800;">
                <th colspan="2">Balance Due</th>
                <th> BHD {{ $invoice->due_amount }}</th>
            </tr>
            </tfoot>
        </table>



    </div>
</div>
<div id="inv_footer">
    <div class="inv_wrapper">
        <div id="pay_info">
            <div class="title">PAYMENT DETAILS   </div>
            <div class="inv_detail_line "><span> Account Name</span><span>:</span><span class="inv_date">Insibe Technologies Co. WLL </span></div>
            <div class="inv_detail_line "><span>Account No</span><span>:</span><span class="inv_date"> 021020084226 </span></div>
            <div class="inv_detail_line "><span>Bank</span><span>:</span><span class="inv_date"> KFH, Adliya Branch </span></div>
            <div class="inv_detail_line "><span>IBAN</span><span>:</span><span class="inv_date"> BH61 KFHO 0002 10200 84226 </span></div>
        </div>



    </div>
</div>
</body>
</html>