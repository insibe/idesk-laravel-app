<html>
<head>

    <style type="text/css" media="all">

       *{
           color: #111;
       }
       body {

           font-family: 'DIN', Sans-serif;
       }
       h1,h2,h3,h4,h5,h6{
            color:#7a2a7e;
           font-family: 'DIN', Sans-serif;
           }
        section{

            padding: 30px;
        }

        .client_info{
            position: absolute;
            right: 0;
            }

       #toc_container {
           background: #f9f9f9 none repeat scroll 0 0;
           border: 1px solid #aaa;
           display: table;
           font-size: 95%;
           margin-bottom: 1em;
           padding: 20px;
           width: auto;
       }

       .toc_title {
           font-weight: 700;
           text-align: center;
       }

       #toc_container li, #toc_container ul, #toc_container ul li{
           list-style: outside none none !important;
       }

       @page {
           header: page-header;
           footer: page-footer;
           background-image: url("http://insibe.net/cover.jpg");
           background-size: cover;
           background-position: 50% 0;
           background-repeat: no-repeat;

           }
   </style>
</head>
<body>
<htmlpageheader  name="page-header">

</htmlpageheader>
<htmlpagefooter name="page-footer">
    {PAGENO}
</htmlpagefooter>


<div class="document_page">
    <div style="margin-top: 5cm">
        <h1><span>Proposal for </span> <br>{{$proposal->proposal_title}} </h1>
        <strong>Ref: #{!! $proposal->proposal_number !!}</strong>
    </div>
    <div style="margin-top: 10cm">
        <div class="client_info" style="float: right; width: 50%;">
            <strong>Prepared For</strong><br>
            @if ($proposal->client->primary_contact)
                {!! $proposal->client->primary_contact!!} -
            @endif
            @if ($proposal->client->company )
                <span>  {!! $proposal->client->company !!}</span><br>
            @endif
            @if ($proposal->client->address1 )
                <span>  {!! $proposal->client->address1 !!}</span>,
            @endif
            @if ($proposal->client->address2 )
                <span>  {!! $proposal->client->address2 !!}</span> <br>
            @endif
            @if ($proposal->client->address2 )
                <span>  {!! $proposal->client->address2 !!}</span><br>
            @endif
            @if ($proposal->client->work_phone )
                <span> P. {!! $proposal->client->work_phone  !!}</span> |
            @endif
            @if ($proposal->client->email )
                <span> E. {!! $proposal->client->email  !!}</span> <br>
            @endif

            <br>
            <strong>Proposal Date</strong><br>
           {!! $proposal->proposal_date !!}

            <br>

        </div>

        <div class="company_info"style="float: left; width: 50%;">
            <strong>Prepared By</strong><br>
            Insibe Technologoies Co.WLL<br>
            No.26,Building 1105,Road No.2721,<br>
            Block No.327, Adliya.<br>
            Kingdom of Bahrain<br><br>
            M. +973 35928671 | 66395959 | 37233959<br>
            E.mail@insibe.com | W.www.insibe.com<br>
        </div>

        <div style="clear: both; margin: 0pt; padding: 0pt; "></div>
    </div>
    <div class="proposal-statement" style="font-size:11px; float: left; width: 100%; margin-top: 5cm" >
        <h6>Statement of Confidentiality</h6>
        <p>This proposal and supporting materials contain confidential and proprietary business information of Insibe Technologies Co.WLL. These materials may be printed or photocopied for use in evaluating the proposed project, but are not to be shared with other parties.</p>
    </div>

    <pagebreak>
   <div class="proposal_content" style="line-height: 1.6em">
       {!! $proposal->proposal_content !!}
   </div>


        <div class="proposal_pricing">
            <h2>Project Pricing</h2>
            <p>Below is the budget we've estimated based on the scope of services outlined earlier in this proposal. If you have any questions about our pricing or need to increase or decrease the scope of work, please leave a comment and let us know.</p>
        </div>

        <div class="document proposal-acceptance ">
            <div class="p-content">

                <h2>Acceptance of Quote</h2>
                <div class="p-details" style="background: #f3f3f3; margin: 0;">
                    <h4 style="clear:both;margin: 0; padding:10px;background: #722a7e;text-align: left; width: 100%; color: #fff;">{!! $proposal->proposal_title !!} #{!! $proposal->proposal_number !!}</h4>
                    <div class="p-info" style="background: #f3f3f3;padding: 10px;width: 100%;">
                        <div class="p-client" style="float: left; width: 60%;" >
                            @if ($proposal->client->primary_contact)
                                <strong>{!! $proposal->client->primary_contact!!} -
                                    @endif
                                    @if ($proposal->client->company )
                                        <span>  {!! $proposal->client->company !!}</span> </strong><br>
                            @endif
                            @if ($proposal->client->address1 )
                                <span>  {!! $proposal->client->address1 !!}</span>,
                            @endif
                            @if ($proposal->client->address2 )
                                <span>  {!! $proposal->client->address2 !!}</span> <br>
                            @endif
                            @if ($proposal->client->address2 )
                                <span>  {!! $proposal->client->address2 !!}</span><br>
                            @endif
                            @if ($proposal->client->work_phone )
                                <span> P. {!! $proposal->client->work_phone  !!}</span> |
                            @endif
                            @if ($proposal->client->email )
                                <span> E. {!! $proposal->client->email  !!}</span> <br>
                            @endif



                        </div>
                        <div class="p-others " style="text-align: right;  ">
                            <h6 style="margin: 0; padding: 0;">Proposal Date</h6>
                            <p style="margin: 0; padding: 0;" >{!! $proposal->proposal_date !!}</p>
                        </div>
                    </div>
                    <table class="table" style="width: 100%; clear: both;">
                        <thead style="width: 100%; clear: both;">>
                        <tr style="width: 100%; clear: both;">>
                            <th style=" text-align: left; padding: 20px;background-color: #f9f9f9;float:left; color: #343a40;font-size: 10px; text-transform: uppercase" >Item Name</th>
                            <th style="  text-align: left;padding: 20px;background-color: #f9f9f9;float:left; color: #343a40;font-size: 10px; text-transform: uppercase">Description</th>
                            <th style="  text-align: left;padding: 20px;background-color: #f9f9f9;float:left; color: #343a40;font-size: 10px; text-transform: uppercase">Pricing</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if (!empty($proposal->ProposalItems))
                            @foreach ($proposal->ProposalItems as $item )
                                <tr>
                                    <td style="border-bottom: 1px solid #c2c2c2; padding: 20px;vertical-align: top;" >{{$item->title}}</td>
                                    <td style="font-size:10px;border-bottom: 1px solid #c2c2c2; padding: 20px;vertical-align: top;" >{{$item->description}}</td>
                                    <td style="border-bottom: 1px solid #c2c2c2;  padding: 20px;vertical-align: top;" >BHD {{$item->price}}</td>
                                </tr>
                            @endforeach
                        @else




                            <tr><td  style=" padding: 10px;vertical-align: top;"  colspan="5" class="text-info text-center">No items found.</td></tr>
                        @endif
                        @if ($proposal->subtotal)
                            <tr>
                                <td class="blank"></td>
                                <td style="border-bottom: 1px solid #c2c2c2; padding: 20px;vertical-align: top;"  >Sub Total: </td>
                                <td style="border-bottom: 1px solid #c2c2c2; padding: 20px;vertical-align: top;" >  <strong>{!! $proposal->subtotal!!}  BHD</strong></td>
                            </tr>
                        @endif
                        @if ($proposal->discount != 0)
                            <tr>
                                <td style="border-bottom: 1px solid #c2c2c2; padding: 20px;vertical-align: top;" class="blank"></td>
                                <td style="border-bottom: 1px solid #c2c2c2; padding: 20px;vertical-align: top;" >Discount </td>
                                <td style=" border-bottom: 1px solid #c2c2c2;padding: 20px;vertical-align: top;"  >  <strong>{!! $proposal->discount!!}  BHD</strong></td>
                            </tr>
                        @endif
                        <tr>
                            <td style=" padding: 20px;vertical-align: top;"  class="blank"></td>
                            <td style=" padding: 20px;vertical-align: top;" >Total </td>
                            <td style=" padding: 20px;vertical-align: top;" >  <strong>{!! $proposal->amount!!}  BHD</strong></td>
                        </tr>

                        </tbody>
                    </table>
                    <div class="p-info" style="padding: 10px;">
                        <p>Our signature below indicates acceptance of this proposal. We would grateful if you would acknowledge the acceptance of this proposal by signing one copy in the space provided below and return one copy to us with a purchase order and advance payment.</p>
                    </div>
                </div>


                <div class="p-info" style="clear: both">
                    <div class="p-client" style="float: left;width: 50%;">
                        <p style="height: 60px"></p>

                        <p>-----------------------------------</p>
                        @if ($proposal->client->primary_contact)
                            <strong>{!! $proposal->client->primary_contact!!} <br>
                                @endif
                                @if ($proposal->client->company )
                                    <span>  {!! $proposal->client->company !!}</span> </strong><br>
                        @endif

                    </div>
                    <div class="p-others" style="float: right;width: 50%; text-align: right">
                        <p style="height: 60px"></p>
                        <p>-----------------------------------</p>
                        <p><strong>Bribin Mathew Philip</strong><br>
                            Project Lead<br>
                            Insibe Technologies Co.Wll</p>
                    </div>
                </div>
            </div>
        </div>
            <pagebreak>
                <div style="clear: both; margin: 0pt; padding: 0pt; "></div>
        <div class="back-cover" >



           <div class="logo_backcover" style="padding-top:90mm;">
               <img style="margin-left: 350px; " src="http://insibe.net/logo_icon.png" width="150" />
               <h1 style="text-transform:uppercase; font-size: 36px; margin-top: -40px; margin-left: 210px;" > Thank You</h1>
           </div>
            <div class="company-info" style="">
                Insibe Technologoies Co.WLL<br>
                CR.No - 117003-1 <br>
                No.26,Building 1105,Road No.2721,<br>
                Block No.327, Adliya.<br>
                Kingdom of Bahrain<br><br>
                E. mail@insibe.com | W. www.insibe.com<br>
                M. +973 35928671 | 66395959 | 38827869 <br>



            </div>

        </div>
    <!-- /.col -->
</div>
</body>
</html>

