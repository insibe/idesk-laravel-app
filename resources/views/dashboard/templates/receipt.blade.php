<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Insibe-</title>
    <style type="text/css">
        @page{margin: 50px 0}
        body{font-family: 'Inter UI', sans-serif; font-size: 12px; line-height: 1.7em;}
        .inv_wrapper{width: 80%; margin: 0 auto}
        #inv_logo{display: inline-block; float:left; width: 50%; vertical-align: middle}
        #inv_address{float:right;width: 50%; text-align: right;}
        #inv_details{background: #f8f8f8; padding: 30px 0;}
        #inv_detialsLft{display: inline-block; float:left; width: 60%; vertical-align: middle}
        #inv_detialsRgt{display: inline-block; float:right; width: 30%; vertical-align: middle}
        #pay_info{padding: 10px;border: 1px solid #f8f8f8; float: left; width: 50%; s;}
        #inv_info{padding: 10px;border: 1px solid #f2f2f2; float: right; width: 50%; background: #f8f8f8;}

        #inv_table { width:100%; margin:30px 0; font-size: 15px; }
        #inv_table th { font-weight:600; }
        #inv_table thead th { background-color:#D9D9D9; font-size:14px; text-align:center; vertical-align:middle; border-right:dashed 1px #fff; }
        #inv_table thead th:last-child { border-right:none; }
        #inv_table tbody td { border-bottom:dashed 1px #d9d9d9; border-right:dashed 1px #d9d9d9; vertical-align:middle; }
        #inv_table tbody td:last-child { border-right:none; }
        #inv_table tbody tr:nth-child(even) { background-color:#FAFAFA; }
        #inv_table tbody tr:last-child td { border-bottom:dashed 1px #BBB; }
        #inv_table tbody td:nth-child(1) { text-align:center; }
        #inv_table tbody td:nth-child(2) { text-align:left; }
        #inv_table tbody td:nth-child(3) { text-align:right; }
        #inv_table tbody td:nth-child(4) { text-align:right; }
        #inv_table tbody td:nth-child(5) { text-align:right; }
        #inv_table tfoot th { vertical-align:middle; text-align:right; }
        #inv_table tfoot th:last-child { border-top:none; border-bottom:solid 1px #d9d9d9; background-color:#fafafa; }
        #inv_table tfoot tr:first-child th:last-child { background-color:#fafafa; }
        #inv_table tfoot tr:last-child th:last-child { border-bottom:double 3px #BBB; background-color:#eee; font-size:16px; }
        #inv_table tfoot th.weak { font-weight:300; text-align:left; vertical-align:bottom; }

    </style>
</head>
<body>
<div id="inv_header">
    <div class="inv_wrapper">
        <div id="inv_logo">
            <img width="200" src="http://insibe.com/assets/img/solutions/inox-accounting-invocing-system-bahrain.png">
        </div>
        <div id="inv_address"><!-- LLPIN Reg No: L01094531<br> -->
            Insibe Technologies Co.WLL | CR No.117003-1<br>
            PO Box 3045 | Kingdom of Bahrain<br>
            M. +973 35928671 , 37233959<br>
            E. mail@insibe.com | W. www.insibe.com
        </div>
    </div>
</div><!-- #inv_invoice -->
<div id="inv_details">
    <div class="inv_wrapper">
        <div id="inv_detialsLft" >
            <div class="font-weight-semibold">BILLED TO</div>
            @if ($retainerInvoice->client->primary_contact)
                {!! $retainerInvoice->client->primary_contact!!} -
            @endif
            @if ($retainerInvoice->client->company )
                <span>  {!! $retainerInvoice->client->company !!}</span><br>
            @endif
            @if ($retainerInvoice->client->address1 )
                <span>  {!! $retainerInvoice->client->address1 !!}</span>,
            @endif
            @if ($retainerInvoice->client->address2 )
                <span>  {!! $retainerInvoice->client->address2 !!}</span> <br>
            @endif
            @if ($retainerInvoice->client->address2 )
                <span>  {!! $retainerInvoice->client->address2 !!}</span><br>
            @endif

            @if ($retainerInvoice->client->work_phone )
                <span> P. {!! $retainerInvoice->client->work_phone  !!}</span> |
            @endif
            @if ($retainerInvoice->client->email )
                <span> E. {!! $retainerInvoice->client->email  !!}</span> <br>
            @endif
        </div>
        <div id="inv_detialsRgt">
            <span class="font-weight-semibold">RETAINER INFO</span>
                <table style="padding: 0; margin: 0">
                    <tr><td width="100px">Retainer#</td><td class="font-weight-semibold">{{$retainerInvoice->rt_number}}</td></tr>
                    <tr><td>Date </td><td class="font-weight-semibold">{{$retainerInvoice->rt_date}}</td></tr>
                    <tr><td>Amount</td><td class="font-weight-semibold">{{$retainerInvoice->rt_amount}}</td></tr>
                </table>
        </div>
    </div>

</div>
<div id="rt-detials" style="font-size: 18px; margin-top: 30px; margin-bottom: 30px;">
    <div class="inv_wrapper">
        <div id="inv_detialsLft">
            <table>
                <tr style="padding: 10px;"><td width="200"  style="color: #777777; font-size: 18px;">Payment Date</td><td style=" border-bottom: 1px dashed #f2f2f2; width: 100%; font-size: 18px;">{{$retainerInvoice->rt_number}}</td></tr>
                <tr><td width="200" style="color: #777777; font-size: 18px;" >Reference Number </td><td style="font-size: 18px;">{{$retainerInvoice->rt_date}}</td></tr>
                <tr><td width="200" style="color: #777777; font-size: 18px;">Payment Mode</td><td style="font-size: 18px;">{{$retainerInvoice->rtPayment_type}}</td></tr>
            </table>
        </div>
        <div id="inv_detialsRgt">
            <div  class="retainerAmount" style="width: 200px; height: 100px; background: #f8f8f8; text-align: center; vertical-align: middle">
                <p>Amount Received</p>
                <h3 class="font-weight-semibold">BHD {{$retainerInvoice->rt_amount}}</h3>
            </div>
        </div>
    </div>
</div>
<hr>
</body>
</html>