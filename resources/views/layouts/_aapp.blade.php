<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="{{ app()->getLocale() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="{{asset('dist/global_assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('dist/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('dist/css/bootstrap_limitless.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('dist/css/layout.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('dist/css/components.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('dist/css/colors.min.css')}}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="{{asset('dist/global_assets/js/main/jquery.min.js')}}"></script>
    <script src="{{asset('dist/global_assets/js/main/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('dist/global_assets/js/plugins/loaders/blockui.min.js')}}"></script>
    <script src="{{asset('dist/global_assets/js/plugins/ui/sticky.min.js')}}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="{{asset('assets/js/app.js')}}"></script>
    <!-- /theme JS files -->
    <script src="{{asset('dist/global_assets/js/demo_pages/components_scrollspy.js')}}"></script>
</head>

<body>

<!-- Main navbar -->
<div class="navbar navbar-expand-md navbar-light fixed-top"">

<!-- Header with logos -->
<div class="navbar-header navbar-dark d-none d-md-flex align-items-md-center">
    <div class="navbar-brand navbar-brand-md">
        <a href="../full/index.html" class="d-inline-block">
            <img src="dist/global_assets/images/logo_light.png" alt="">
        </a>
    </div>

    <div class="navbar-brand navbar-brand-xs">
        <a href="../full/index.html" class="d-inline-block">
            <img src="/dist/global_assets/images/logo_icon_light.png" alt="">
        </a>
    </div>
</div>
<!-- /header with logos -->


<!-- Mobile controls -->
<div class="d-flex flex-1 d-md-none">
    <div class="navbar-brand mr-auto">
        <a href="../full/index.html" class="d-inline-block">
            <img src="../../../../global_assets/images/logo_dark.png" alt="">
        </a>
    </div>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
        <i class="icon-tree5"></i>
    </button>

    <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
        <i class="icon-paragraph-justify3"></i>
    </button>
</div>
<!-- /mobile controls -->


<!-- Navbar content -->
<div class="collapse navbar-collapse" id="navbar-mobile">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                <i class="icon-paragraph-justify3"></i>
            </a>
        </li>

        <li class="nav-item">
            <a href="#" class="navbar-nav-link">Text link</a>
        </li>

        <li class="nav-item dropdown">
            <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">Menu</a>

            <div class="dropdown-menu">
                <a href="#" class="dropdown-item">Action</a>
                <a href="#" class="dropdown-item">Another action</a>
                <a href="#" class="dropdown-item">One more action</a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">Separate action</a>
            </div>
        </li>
    </ul>

    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a href="#" class="navbar-nav-link">Text link</a>
        </li>

        <li class="nav-item dropdown">
            <a href="#" class="navbar-nav-link">
                <i class="icon-bell2"></i>
                <span class="d-md-none ml-2">Notifications</span>
                <span class="badge badge-mark border-white"></span>
            </a>
        </li>

        <li class="nav-item dropdown dropdown-user">
            <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                <img src="../../../../global_assets/images/image.png" class="rounded-circle" alt="">
                <span>Victoria</span>
            </a>

            <div class="dropdown-menu dropdown-menu-right">
                <a href="#" class="dropdown-item"><i class="icon-user-plus"></i> My profile</a>
                <a href="#" class="dropdown-item"><i class="icon-coins"></i> My balance</a>
                <a href="#" class="dropdown-item"><i class="icon-comment-discussion"></i> Messages <span class="badge badge-pill bg-blue ml-auto">58</span></a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item"><i class="icon-cog5"></i> Account settings</a>
                <a href="#" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
            </div>
        </li>
    </ul>
</div>
<!-- /navbar content -->

</div>
<!-- /main navbar -->


<!-- Page content -->
<div class="page-content">

    <!-- Main sidebar -->
    <div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

        <!-- Sidebar mobile toggler -->
        <div class="sidebar-mobile-toggler text-center">
            <a href="#" class="sidebar-mobile-main-toggle">
                <i class="icon-arrow-left8"></i>
            </a>
            Navigation
            <a href="#" class="sidebar-mobile-expand">
                <i class="icon-screen-full"></i>
                <i class="icon-screen-normal"></i>
            </a>
        </div>
        <!-- /sidebar mobile toggler -->


        <!-- Sidebar content -->
        <div class="sidebar-content">

            <!-- Main navigation -->
            <div class="card card-sidebar-mobile">
                <ul class="nav nav-sidebar" data-nav-type="accordion">

                    <!-- Main -->
                    <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>
                    <li class="nav-item">
                        <a href="/dashboard/clients" class="nav-link"><i class="fas fa-user-tie"></i><span>Clients</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="/dashboard/proposals" class="nav-link"><i class="far fa-file-pdf"></i><span>Proposals</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="/dashboard/advance-payments" class="nav-link"><i class="fas fa-briefcase"></i><span>Advance Payments</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="/dashboard/invoices" class="nav-link"><i class="fas fa-file-invoice"></i><span>Invoices</span></a>
                    </li>
                    <!-- /main -->

                </ul>
            </div>
            <!-- /main navigation -->

        </div>
        <!-- /sidebar content -->

    </div>
    <!-- /main sidebar -->


    <!-- Main content -->
    <div class="content-wrapper">

    @yield('toolbar')


    <!-- Content area -->
        <div class="content pt-0">

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @yield('content')

        </div>
        <!-- /content area -->


        <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Footer
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-footer">
					<span class="navbar-text">
						&copy; 2015 - 2018. <a href="#">Insibe Technologies Co.WLL</a>
					</span>

                <ul class="navbar-nav ml-lg-auto">
                    <li class="nav-item">
                        <a href="#" class="navbar-nav-link">Text link</a>
                    </li>

                    <li class="nav-item">
                        <a href="#" class="navbar-nav-link">
                            <i class="icon-lifebuoy"></i>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="#v" class="navbar-nav-link font-weight-semibold">
								<span class="text-pink-400">
									<i class="icon-cart2 mr-2"></i>
									Purchase
								</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /footer -->

    </div>
    <!-- /main content -->

</div>
<!-- /page content -->

</body>
</html>
