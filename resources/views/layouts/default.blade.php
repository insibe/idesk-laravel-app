<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/AdminLTE.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/skins/skin-blue.css')}}">
    <script src="{{ asset('assets/js/jquery-1.11.2.min.js')}}"></script>
    <script src="{{ asset('assets/js/popper.min.js')}}"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:400,700,800" rel="stylesheet">

</head>

<body>

<div class="d-flex align-items-center justify-content-center ht-100v">
    @yield('login')
</div><!-- d-flex -->

<script src="{{ asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('assets/js/adminlte.min.js')}}"></script>
</body>
</html>
